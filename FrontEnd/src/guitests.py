from selenium import webdriver
from unittest import main, TestCase
import time


class TestCases(TestCase):
    def setUp(self):
        chromedriver = "./chromedriver"
        self.driver = webdriver.Chrome(chromedriver)
        self.driver.get("https://www.volunteer4.me")

    # Test Writer: Shivam Patel
    # Test title of Home
    def test_title(self):
        self.driver.get("https://www.volunteer4.me")
        self.assertEqual(self.driver.title, "Volunteer4.Me")

    # Test Writer: Shivam Patel
    # Test Home Nav Link
    def test_home(self):
        self.driver.get("https://www.volunteer4.me")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[1]/a"
        )
        self.assertEqual(button.get_attribute("innerHTML"), "Home")
        button.click()
        self.assertEqual(self.driver.current_url, "https://www.volunteer4.me/")

    # # Test Writer: Shivam Patel
    # # Test About Nav Link
    def test_about(self):
        self.driver.get("https://www.volunteer4.me")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[5]/a"
        )
        self.assertEqual(button.get_attribute("innerHTML"), "About")
        button.click()
        self.assertEqual(self.driver.current_url, "https://www.volunteer4.me/About")

    # # Test Writer: Shivam Patel
    # # Test All Events Nav Link
    def test_all_events(self):
        self.driver.get("https://www.volunteer4.me")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[2]/a"
        )
        self.assertEqual(button.get_attribute("innerHTML"), "All Events")
        button.click()
        self.assertEqual(self.driver.current_url, "https://www.volunteer4.me/AllEvents")

    # # Test Writer: Shivam Patel
    # # Test Types of Events Nav Link
    def test_types(self):
        self.driver.get("https://www.volunteer4.me")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[3]/a"
        )
        self.assertEqual(button.get_attribute("innerHTML"), "Types of Events")
        button.click()
        self.assertEqual(
            self.driver.current_url, "https://www.volunteer4.me/TypesOfEvents"
        )

    # # Test Writer: Shivam Patel
    # # Test Community Groups Nav Link
    def test_com_groups(self):
        self.driver.get("https://www.volunteer4.me")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[4]/a"
        )
        self.assertEqual(button.get_attribute("innerHTML"), "Community Groups")
        button.click()
        self.assertEqual(
            self.driver.current_url, "https://www.volunteer4.me/CommunityGroups"
        )

    # Test Writer: Shivam Patel
    # Test All Events Page for Cards Present
    def test_all_events_cards(self):
        self.driver.get("https://www.volunteer4.me/AllEvents")
        exists = len(self.driver.find_elements_by_class_name("card-deck")) > 0
        self.assertEqual(True, exists)

    # # Test Writer: Shivam Patel
    # # Test Types of Events for Cards Present
    def test_event_type_cards(self):
        self.driver.get("https://www.volunteer4.me/TypesOfEvents")
        exists = len(self.driver.find_elements_by_class_name("card-deck")) > 0
        self.assertEqual(True, exists)

    # Test Writer: Shivam Patel
    # Test Footer Presence on Home and Model Pages
    def test_footer(self):
        self.driver.get("https://www.volunteer4.me/")
        footer = self.driver.find_element_by_xpath("//*[@class='footer-container']/h6")
        self.assertEqual(
            footer.get_attribute("innerHTML"), "Copyright © 2019 Volunteer4Me"
        )
        self.driver.get("https://www.volunteer4.me/CommunityGroups")
        footer = self.driver.find_element_by_xpath("//*[@class='footer-container']/h6")
        self.assertEqual(
            footer.get_attribute("innerHTML"), "Copyright © 2019 Volunteer4Me"
        )
        self.driver.get("https://www.volunteer4.me/TypesOfEvents")
        footer = self.driver.find_element_by_xpath("//*[@class='footer-container']/h6")
        self.assertEqual(
            footer.get_attribute("innerHTML"), "Copyright © 2019 Volunteer4Me"
        )
        self.driver.get("https://www.volunteer4.me/AllEvents")
        footer = self.driver.find_element_by_xpath("//*[@class='footer-container']/h6")
        self.assertEqual(
            footer.get_attribute("innerHTML"), "Copyright © 2019 Volunteer4Me"
        )

    # # Test Writer: Shivam Patel
    # # Test Workflow of following first event page
    def test_single_event_card(self):
        self.driver.get("https://www.volunteer4.me/AllEvents")
        button = self.driver.find_element_by_xpath(
            "//*[@id='root']/div/div[2]/div/div[2]/div[1]/div/div/div[1]/b/span/span"
        )
        self.assertEqual(
            button.get_attribute("innerHTML"),
            "Paso Robles Wine Tasting Trip and Tour. All Inclusive!!! Oh What Fun!",
        )

    # # Test Writer: Shivam Patel
    # # Test Workflow of following first event page
    def test_search(self):
        self.driver.get("https://www.volunteer4.me/AllEvents")

        button = self.driver.find_element_by_xpath(
            "//*[@id='root']/div/div[2]/div/div[1]/div[5]/input"
        )
        button.send_keys("Henry")
        time.sleep(3)
        button = self.driver.find_element_by_xpath(
            "//*[@id='root']/div/div[2]/div/div[2]/div[1]/div/div/div[1]/b/span/span"
        )
        self.assertEqual(
            button.get_attribute("innerHTML"),
            "Paso Robles Wine Tasting Trip and Tour. All Inclusive!!! Oh What Fun!",
        )

    def test_active_allevents(self):
        self.driver.get("https://www.volunteer4.me/AllEvents")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[2]/a"
        )
        cls = button.get_attribute("class")
        lis = cls.split(" ")
        self.assertEqual(lis[0], "active")

    def test_active_eventtypes(self):
        self.driver.get("https://www.volunteer4.me/TypesOfEvents")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[3]/a"
        )
        cls = button.get_attribute("class")
        lis = cls.split(" ")
        self.assertEqual(lis[0], "active")

    def test_active_comgroups(self):
        self.driver.get("https://www.volunteer4.me/CommunityGroups")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[4]/a"
        )
        cls = button.get_attribute("class")
        lis = cls.split(" ")
        self.assertEqual(lis[0], "active")

    def test_active_about(self):
        self.driver.get("https://www.volunteer4.me/About")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[5]/a"
        )
        cls = button.get_attribute("class")
        lis = cls.split(" ")
        self.assertEqual(lis[0], "active")

    def test_active_home(self):
        self.driver.get("https://www.volunteer4.me/")
        button = self.driver.find_element_by_xpath(
            "//*[@id='basic-navbar-nav']/div/div[1]/a"
        )
        cls = button.get_attribute("class")
        lis = cls.split(" ")
        self.assertEqual(lis[0], "active")

    # Test Writer: Shivam Patel
    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    main()
