import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './Components/Home.js';
import Navbar from './Components/CustomNavbar.js'
import Footer from './Components/Footer.js'
import About from './Components/About.js';
import AllEvents from './Components/AllEvents.js';
import TypesOfEvents from './Components/TypesOfEvents.js';
import CommunityGroups from './Components/CommunityGroups.js';
import Event from './Components/Event.js';
import Event1Types from './Components/Event1Types.js';
import CommunityGroupsInstance from './Components/CommunityGroupsInstance.js';
import SearchedEvents from "./Components/SearchedEvents.js"
import Vis from "./Components/Vis.js"
import CusVis from "./Components/CusVis.js"

class App extends Component {
  render() {
    return (

      <Router>
        <div>
          <Navbar/>
          <Route exact path="/" component={Home}/>
          <Route path ="/About" component ={About}/>
          <Route path ="/AllEvents" component ={AllEvents}/>
          <Route path ="/TypesOfEvents" component ={TypesOfEvents}/>
          <Route path ="/CommunityGroups" component ={CommunityGroups}/>
          <Route path = "/Event/:EventNum" component={Event}/>
          <Route path = "/Event1Types/:EventTypeNum" component={Event1Types}/>
          <Route path = "/CommunityGroupsInstance/:GroupId" component={CommunityGroupsInstance}/>
          <Route path = "/SearchedEvents/:searchTerm" component={SearchedEvents}/>
          <Route path = "/Vis" component={Vis}/>
          <Route path = "/CusVis" component={CusVis}/>
          <Footer/>
        </div>
      </Router>
    );
  }
}

export default App;
