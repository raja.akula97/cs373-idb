import React, { Component } from 'react';
import Pagination from './Pagination';

import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, ButtonToolbar} from 'reactstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import CommunityGroupsInstance from './CommunityGroupsInstance.js';
import './CommunityGroups.css';
import SearchField from "react-search-field";
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Button2 from 'react-bootstrap/Button';
import Highlighter from "react-highlight-words";
import StarRatings from 'react-star-ratings';
import Select from 'react-select'

const options = [
    { value: 'large', label: 'Large (> 10,000)' },
    { value: 'medium', label: 'Medium (200 - 10,000)' },
    { value: 'small', label: 'Small (< 200)' }
  ]

const ratings = [
    { value: '5.0', label: '5.0' },
    { value: '4.0', label: '> 4.0' },
    { value: '3.0', label: '> 3.0' },
    { value: '2.0', label: '> 2.0' },
    { value: '1.0', label: '> 1.0' }
  ]

const sorts = [
    { value: 'group true', label: 'Group name: A-Z' },
    { value: 'group false', label: 'Group name: Z-A' },
    { value: 'ec true', label: 'Event Count: Low to High'},
    { value: 'ec false', label: 'Event Count: High to Low'},
    { value: 'mc true', label: 'Member Count: Low to High'},
    { value: 'mc false', label: 'Member Count: High to Low'}

  ]

var globalURL = "https://api.volunteer4.me/communityGroups?all=True"
var globalEvent = ""

var prevSearch = ""

var dictURL = {
  member_count: "",
  rating: "",
  sort: "",
  search: ""
};

var searchTerm = "";

class CommunityGroups extends Component{

   constructor() {
        super();
        this.state = {
            pageOfItems: [],
            exampleItems: [],
            response: []
        };

        // bind function in constructor instead of render (https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)
        this.onChangePage = this.onChangePage.bind(this);
        this.nameSort = this.nameSort.bind(this);
        this.filtering = this.filtering.bind(this);
        this.memberCountFilter = this.memberCountFilter.bind(this);
        this.ratingsFilter = this.ratingsFilter.bind(this);
        this.initialRender = this.initialRender.bind(this);
        this.searchFilter = this.searchFilter.bind(this);
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handle(resp) {
        this.setState( {
            response: resp
        });
    }

    exampItems(resp) {
        this.setState( {
            exampleItems: resp
        });
    }

    componentDidMount() {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', 'https://api.volunteer4.me/communityGroups?all=True', true)
        var st = this
        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
                //return server response as an object with JSON.parse
                var resp = JSON.parse(this.responseText)
                st.handle(resp)
                var length = Object.keys(resp).length;
                var exampleItems = [...Array(length).keys()].map(i => ({
                id: resp[i]['id'],
                img: resp[i]["photo_url"],
                com_group: resp[i]["community_group_name"],
                event_count: resp[i]["num_events"],
                rating: resp[i]["rating"],
                member_count: resp[i]["Member Count"],
                description: resp[i]["description"],
                link: "CommunityGroupsInstance/"}));
                st.exampItems(exampleItems)
            }
        }
        xhr.send();


    }

    initialRender() {
        window.location.reload()
        dictURL["member_count"] = '';
        dictURL["rating"] = '';
        dictURL["sort"] = '';
        dictURL["search"] = '';
        this.componentDidMount()
    }

    nameSort(ascending){
        let xhr = new XMLHttpRequest;
        var st = this
        var val = true
        var sortKind = ascending['value'].split(' ')
        var resultString = '&sortby='
        if (sortKind[0] == 'group'){
            resultString+='name'
        }
        else if (sortKind[0] == 'ec'){
            resultString+='num_events'
        }
        else if (sortKind[0] == 'mc'){
            resultString+='member_count'
        }
        if (sortKind[1] == 'false'){
            resultString += '&sortrev=True'
        }
        dictURL["sort"] = resultString
        st.filtering(xhr, st);
    }

    memberCountFilter(category) {
        if(category != null) {
            var encoded_uri = encodeURIComponent(category['value'])
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["member_count"] = '&member_count=' + encoded_uri

            st.filtering(xhr, st)
        } else {
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["member_count"] = ''

            st.filtering(xhr, st)
        }
    }

    ratingsFilter(num) {
        if(num != null) {
            var encoded_uri = encodeURIComponent(num['value'])
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["rating"] = '&rating=' + encoded_uri

            st.filtering(xhr, st)
        } else {
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["rating"] = ''

            st.filtering(xhr, st)
        }
    }

    searchFilter(keywordVal) {
        if(prevSearch.length > 0 && this.search5.value.length == 0) {
            prevSearch = this.search5.value
            window.location.reload()
        } else {
            var encoded_uri = encodeURIComponent(this.search5.value)
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["search"] = '&search=' +  encoded_uri
            searchTerm = encoded_uri

            st.filtering(xhr, st)
        }
    }

    filtering(xhr, st) {
        var url = globalURL + dictURL["sort"] + dictURL["rating"]  + dictURL["member_count"] + dictURL["search"]
        xhr.open('GET', url, true)
        var resp = null;

        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var length = Object.keys(resp).length
              var exampleItems = [...Array(length).keys()].map(i => ({
                img: resp[i]["photo_url"],
                com_group: resp[i]["community_group_name"],
                event_count: resp[i]["num_events"],
                rating: resp[i]["rating"],
                member_count: resp[i]["Member Count"],
                description: resp[i]["description"],
                id: resp[i]["id"],
                link: "CommunityGroupsInstance/"}));
              st.exampItems(exampleItems)
            }
        }
        xhr.send();
    }

   render() {
        return(

               <div className="root">
                <div className="container">
                <br></br><br></br><br></br>

                <div className = "filter_section" class="row">
                    <div class="col-sm">
                        <Select placeholder="Member Count" options={options} onChange={this.memberCountFilter}/>
                    </div>
                    <div class="col-sm">
                    <Select placeholder="Rating" options={ratings} onChange={this.ratingsFilter}/>
                    </div>
                    <div class="col-sm">
                    <Select placeholder="Sort" options={sorts} onChange={this.nameSort}/>
                    </div>
                    <div class="col-sm">
                        <input className="form-control mr-sm-2" onChange={this.searchFilter} placeholder="Search groups..." ref={input => this.search5 = input}/>
                    </div>
                    <div class="col-md-auto">
                        <Button2  variant="outline-danger" onClick={this.initialRender}>Reset Filters</Button2>
                    </div>

                </div>
                <p></p>
                    <CardDeck>
                    {this.state.pageOfItems.map(item =>
                     <div class="col-4">
                     <Card raised className="main-card">
                     <CardImg src={item.img} height="200px"></CardImg>
                     <CardBody>
                        <CardTitle><b><Highlighter
                                highlightClassName="Active"
                                searchWords={[String(searchTerm)]}
                                autoEscape={true}
                                textToHighlight={item.com_group}
                            /></b>
                        </CardTitle>
                        <CardText className="date"><b>Group Rating:</b>
                                <StarRatings
                                        rating={item.rating}
                                        starDimension="20px"
                                        starSpacing="3px"
                                />
                                <br/><b>
                        Number of Events in the Group:</b> {item.event_count}
                        <br/> <b>Member Count:</b> {item.member_count} </CardText>
                        <a href={item.link + item.id}>Details</a>
                    </CardBody>

                     </Card><br/>
                     </div>

                     )}
                    </CardDeck>
                    <br></br><br></br>
                    <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage} />
                </div>
               </div>
			);

	}
}
export default CommunityGroups;
