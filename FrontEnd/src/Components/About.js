import React, { Component } from 'react';
import GitTable from './GitTable'
import "./About.css"

class About extends Component{
	render() {
		return(
			<div class="root">
			<div class="container">
				<h1>About Us</h1>
                <br/>
				<br></br>
				<h4>Why Volunteering?</h4>
				<div class="col-lg">
					<p>Volunteering is such an important part of our communities, and it’s our duty as citizens to improve them through acts of service. Volunteer4.me helps 
					people in the Austin area easily find upcoming volunteering opportunities based on their communities and interests. Let’s work together to help society!</p>
					<p>We connect and work with different data which allows us to guide you to your desired volunteer opportunities! This integration helps filter and match 
					you up based on specifics since we have different datasets to work with!</p>
				</div>
                <br/>
				<h4>Team Members</h4>
				<div>
					<div class="row">
						<div class="col">
							<div class="card" style={{width: '350px', height: 'auto'}}>
								<div class="card-body">
									<img class="card-img-top img-fluid" style={{width: '300px', height: 'auto'}} src={require('./img/nishtha.jpg')} alt="Nishtha's Profile Image"></img>
									<div class="card-body fluid">
										<h5 class="card-title">Nishtha Aggarwal</h5>
										<h7 class="card-title">Roles: Frontend Engineer</h7>
										<p class="card-text">Junior Computer Science Major at UT Austin (Graduating May 2020) who loves concerts and baking</p>
										<a href="https://www.linkedin.com/in/nishtha-aggarwal-339bb2128/" class="card-link">LinkedIn Profile</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="card" style={{width: '350px', height: 'auto'}}>
								<div class="card-body">
									<img class="card-img-top img-fluid" style={{width: '300px', height: 'auto'}} src={require('./img/kaushik.jpg')} alt="Kaushik's Profile Image"></img>
									<div class="card-body">
										<h5 class="card-title">Kaushik Koirala</h5>
										<h7 class="card-title">Roles: API Data/Config Handling</h7>
										<p class="card-text">Junior Computer Science major at UT Austin who is interested in soccer, politics and movies</p>
										<a href="https://www.linkedin.com/in/kaushik-koirala-097629181/" class="card-link">LinkedIn Profile</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="card" style={{width: '350px', height: 'auto'}}>
								<div class="card-body">
									<img class="card-img-top img-fluid" style={{width: '300px', height: 'auto'}} src={require('./img/haarika.jpg')} alt="Haarika's Profile Image"></img>
									<div class="card-body">
										<h5 class="card-title">Haarika Somarouthu</h5>
										<h7 class="card-title">Roles: Frontend Developer</h7>
										<p class="card-text">Junior Computer Science Major at UT Austin (Graduating May 2020) who loves to dance</p>
										<a href="https://www.linkedin.com/in/haarika-somarouthu-b2b014164/" class="card-link">LinkedIn Profile</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="card" style={{width: '350px', height: 'auto'}}>
								<div class="card-body">
									<img class="card-img-top img-fluid" style={{width: '300px', height: 'auto'}} src={require('./img/raja.jpg')} alt="Raja's Profile Image"></img>
									<div class="card-body">
										<h5 class="card-title">Raja Akula</h5>
										<h7 class="card-title">Roles: Web Server, Backend</h7>
										<p class="card-text">Junior Computer Science Major at UT Austin (Gradiuating Fall 2019) who loves photography</p>
										<a href="https://www.linkedin.com/in/raja-akula-555b03128/" class="card-link">LinkedIn Profile</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="card" style={{width: '350px', height: 'auto'}}>
								<div class="card-body">
									<img class="card-img-top img-fluid" style={{width: '300px', height: 'auto'}} src={require('./img/shivam.jpg')} alt="Shivam's Profile Image"></img>
									<div class="card-body">
										<h5 class="card-title">Shivam Patel</h5>
										<h7 class="card-title">Roles: Git God, Backend</h7>
										<p class="card-text">Junior Computer Science major at UT Austin who loves to travel and is a part of UT Punjabbawockeez</p>
										<a href="https://www.linkedin.com/in/shivam-p-patel/" class="card-link">LinkedIn Profile</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col"></div>
					</div>
				</div>
				<div class="row mt-5">
					<h3>GitLab Statistics</h3>
					<GitTable />
				</div>
				<div class="row mt-5">
					<div class="col">
						<h3 class="text-center"><a href="https://documenter.getpostman.com/view/6805936/S11LsHsW">Postman API Link</a></h3>
					</div>
					<div class="col">
						<h3 class="text-center"><a href="https://gitlab.com/raja.akula97/cs373-idb">GitLab Repo Link</a></h3>
					</div>
				</div>
               <br/>
				<div class="row mt-2">
					<h3>Data Sources</h3>
					<div class = "container">
						<br/>
						<a href="https://www.meetup.com/meetup_api/">Meetup API:</a><br/>
						<p>The Meetup API gave us information on Meetup events and in general in JSON format, which we then had to parse through
						to find specific volunteering opportunities and volunteering oriented groups. Most attributes (such as name, and description) translated directly
						to the data we were able to put on our pages but some things, such as time, needed to be converted for readability. </p>
						<br/>
						<a href="https://serpapi.com/">Serp API:</a><br/>

						<p> Unlike the other two APIs, Serp API allows users to query Google. The search results from Google are provided in a json format, 
						allowing us to grab needed information. We used this to query description attributes for our type.
						</p>  <br/>
						<a href="https://developers.google.com/youtube/v3/">Youtube API:</a><br/>
						<p> This is the Youtube API, which provides information, such as video-id, to implement embedded videos in our instance pages. In the future phases
							we plan on using this API to pull video information, and implement videos using react-youtube package.
						</p>
          				</div>
    					<div>
               </div>
                <br/>
               <br/>
                <br/>
				<div>
				<br></br><br></br>
				<h3>Putting it all together </h3>
				<p></p>
		           <div class="col-lg">
					<p>We used the Meetup API to provide us information regarding the volunteering opportunities, event types, and community groups. We plan on integrating data from other APIs such as Eventbrite to 
					enrich our database with even more opportunities. We have used and plan on using the other two APIs (SerpAPI and YouTube) to better define and visualize what each of our end users 
					can expect in terms of the various opportunities and events. Althogether, we hope this allows for an engaging 
					experience for our users that is persuasive enough to convince them to partake in some of the many events we have presented them.</p>
				</div>
				</div>
                                                                                                                                                                                                                                                                                                                                                                                                 </div>
				<div class="row mt-5">
					<h3>Tools Used</h3>
				</div>
				<div class="row mt-2">
					<div class="col-sm-2">
						<a href="https://about.gitlab.com/"><img className="logo" src="https://docs.gitlab.com/assets/images/gitlab-logo.svg"></img></a>
					</div>
					<div class="col-sm-4">
						<span class="align-middle">GitLab provides us code status and workflow solutions that help us stay organized and push clean code.</span>
					</div>
					<div class="col-sm-2">
						<a href="https://www.namecheap.com/"><img className="logo" src="http://pluspng.com/img-png/logo-namecheap-png-namecheap-domain-and-hosting-coupons-promocodes-480.png"></img></a>
					</div>
					<div class="col-sm-4">
						<span class="align-middle">NameCheap provides our team cheap, easy-to-use domain services to simply connect with users.</span>
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-sm-2">
						<a href="https://aws.amazon.com/"><img className="logo" src="https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png"></img></a>
					</div>
					<div class="col-sm-4">
						<span class="align-middle">AWS is an easy-to-use cloud services platform that serves server hosting and compute power all from cloud instances</span>
					</div>
					<div class="col-sm-2">
						<a href="https://reactjs.org/"><img className="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2000px-React-icon.svg.png"></img></a>
					</div>
					<div class="col-sm-4">
						<span class="align-middle">React is a simple JavaScript framework that expedites the process of creating UI interactions with users over web applications.</span>
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-sm-2">
						<a href="https://www.getpostman.com/"><img className="logo" src="https://www.getpostman.com/img/v2/logo-glyph.png"></img></a>
					</div>
					<div class="col-sm-4">
						<span class="align-middle">Postman was used to easily set up and design our api as well as scrape sample data to catalyze our development process.</span>
					</div>
				</div>
			</div>
			</div>
			);

	}
}
export default About;
