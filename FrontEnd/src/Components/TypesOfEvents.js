import React, { Component } from 'react';
import Pagination from './Pagination';

import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, ButtonToolbar} from 'reactstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import Event1Types from './Event1Types.js';
import "./TypesOfEvents.css";
import SearchField from "react-search-field";
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Button2 from 'react-bootstrap/Button';
import Highlighter from "react-highlight-words";
import StarRatings from 'react-star-ratings';
import Select from 'react-select'
import AsyncSelect from 'react-select/lib/Async';


var globalURL = "https://api.volunteer4.me/eventTypes?all=True"
var globalEvent = ""

var prevSearch = "";

var dictURL = {
  type_id: "",
  rating: "",
  num_events: "",
  num_groups: "",
  sort: "",
  search: ""
};

var searchTerm = "";

const options1 = [
    { value: 'large', label: 'Large (> 50)' },
    { value: 'medium', label: 'Medium (20 - 50)' },
    { value: 'small', label: 'Small (< 20)' }
  ]

const options2 = [
    { value: 'large', label: 'Large (> 50)' },
    { value: 'medium', label: 'Medium (20 - 50)' },
    { value: 'small', label: 'Small (< 20)' }
  ]

const ratings = [
    { value: '4.0', label: '> 4.0' },
    { value: '3.0', label: '> 3.0' },
    { value: '2.0', label: '> 2.0' },
    { value: '1.0', label: '> 1.0' }
  ]

const sorts = [
    { value: 'type true', label: 'Type name: A-Z' },
    { value: 'type false', label: 'Type name: Z-A' },
    { value: 'ec true', label: 'Event Count: Low to High' },
    { value: 'ec false', label: 'Event Count: High to Low' },
    { value: 'gc true', label: 'Group Count: Low to High' },
    { value: 'gc false', label: 'Group Count: High to Low' }
  ]

class TypesOfEvents extends Component{
     constructor() {
        super();

        this.state = {
            exampleItems: {},
            pageOfItems: [],
            response: null,
            exampleStr : "blah",
            query: '',
        };

        this.onChangePage = this.onChangePage.bind(this);
        this.nameSort = this.nameSort.bind(this);
        this.filtering = this.filtering.bind(this);
        this.numGroupsFilter = this.numGroupsFilter.bind(this);
        this.numEventsFilter = this.numEventsFilter.bind(this);
        this.ratingsFilter = this.ratingsFilter.bind(this);
        this.initialRender = this.initialRender.bind(this);
        this.searchFilter = this.searchFilter.bind(this);

    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    componentDidMount() {
        let xhr = new XMLHttpRequest;
        var url = globalURL + dictURL["sort"] + dictURL["type_id"] + dictURL["rating"] + dictURL["num_events"] + dictURL["num_groups"]
        + dictURL["search"]
        var st = this
        xhr.open('GET', url, true)
        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var length = Object.keys(resp).length;
              var exampleItems = [...Array(length).keys()].map(i => ({
                id: resp[i]['id'],
                num_events: resp[i]['num_events'],
                num_groups: resp[i]["num_groups"],
                avg_group_rating: Math.round((resp[i]["avg_group_rating"] * 100))/100,
                event_type: resp[i]["event_type_name"],
                description: resp[i]["description"],
                photo_url: resp[i]["photo_url"],
                id: resp[i]["id"],
                link: "Event1Types/"}));
                st.exampItems(exampleItems)
            }
        }
        xhr.send();
    }


    initialRender() {
        window.location.reload()
        document.getElementById("field1").value = "";
        searchTerm =""
        dictURL["search"] = '';
        this.componentDidMount()
    }

    exampItems(resp) {
        this.setState( {
            exampleItems: resp
        });
    }

    handle(resp) {
        this.setState( {
            response: resp
        });
    }

    nameSort(ascending){
        let xhr = new XMLHttpRequest;
        var st = this
        var val = true
        var sortKind = ascending['value'].split(' ')
        var resultString = '&sortby='
        if (sortKind[0] == 'type'){
            resultString+='name'
        }
        else if (sortKind[0] == 'ec'){
            resultString+='num_events'
        }
        else if (sortKind[0] == 'gc'){
            resultString+='num_groups'
        }
        if (sortKind[1] == 'false'){
            resultString += '&sortrev=True'
        }
        dictURL["sort"] = resultString
        st.filtering(xhr, st)
    }

    numGroupsFilter(category) {
        if (category != null) {
            var encoded_uri = encodeURIComponent(category['value'])
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["num_groups"] = '&num_groups=' + encoded_uri

            st.filtering(xhr, st)
        }
        else {
            dictURL["num_groups"] = ''
            let xhr = new XMLHttpRequest;
            var st = this
            st.filtering(xhr, st)
        }
    }

    numEventsFilter(category) {
        if (category != null) {
            var encoded_uri = encodeURIComponent(category['value'])
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["num_events"] = '&num_events=' + encoded_uri

            st.filtering(xhr, st)
        }
        else {
            dictURL["num_events"] = ''
            let xhr = new XMLHttpRequest;
            var st = this
            st.filtering(xhr, st)
        }
    }

    ratingsFilter(category) {
        if (category != null) {
            var encoded_uri = encodeURIComponent(category['value'])
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["rating"] = '&rating=' + encoded_uri

            st.filtering(xhr, st)
        }
        else {
            dictURL["rating"] = ''
            let xhr = new XMLHttpRequest;
            var st = this
            st.filtering(xhr, st)
        }
    }

    searchFilter(keywordVal) {
        if(prevSearch.length > 0 && this.search5.value.length == 0) {
            prevSearch = this.search5.value
            window.location.reload()
        } else {
            prevSearch = this.search5.value
            var encoded_uri = encodeURIComponent(this.search5.value)
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["search"] = '&search=' +  encoded_uri
            searchTerm = encoded_uri

            st.filtering(xhr, st)
        }
    }

    filtering(xhr, st) {
        var url = globalURL + dictURL["sort"] + dictURL["type_id"] + dictURL["rating"] + dictURL["num_events"] + dictURL["num_groups"]
        + dictURL["search"]
        xhr.open('GET', url, true)
        var resp = null;
        var s = this.state

        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var length = Object.keys(resp).length
              var exampleItems = [...Array(length).keys()].map(i => ({
                num_events: resp[i]['num_events'],
                num_groups: resp[i]["num_groups"],
                avg_group_rating: Math.round((resp[i]["avg_group_rating"] * 100))/100,
                event_type: resp[i]["event_type_name"],
                description: resp[i]["description"],
                photo_url: resp[i]["photo_url"],
                id: resp[i]["id"],
                link: "Event1Types/"}));
              st.exampItems(exampleItems)
            }
        }
        xhr.send();
    }

   render() {
       return(

               <div className="root">
               <br></br>
               <br></br>
               <br></br>
               <div class="container">
               <div class="row">

                    <div class="col-sm">
                        <Select placeholder="Num Groups" options={options1} onChange={this.numGroupsFilter}/>
                    </div>
                    <div class="col-sm">
                    <Select placeholder="Num Events" options={options2} onChange={this.numEventsFilter}/>
                    </div>
                    <div class="col-sm">
                    <Select placeholder="Rating" options={ratings} onChange={this.ratingsFilter}/>
                    </div>
                    <div class="col-sm">
                    <Select placeholder="Sort" options={sorts} onChange={this.nameSort}/>
                    </div>
                    <div class="col-sm">
                        <input id="field1" className="form-control mr-sm-2" onChange={this.searchFilter} placeholder="Search types..." ref={input => this.search5 = input}/>
                    </div>
                    <div class="col-md-auto">
                        <Button2  variant="outline-danger" onClick={this.initialRender}>Reset Filters</Button2>
                    </div>

                </div>
                </div>

                <div className="container">
                  <p></p>

                    <CardDeck>
                    {this.state.pageOfItems.map(item =>
                     <div class="col-4">
                     <Card raised className="main-card">
                        <CardImg src={item.photo_url}></CardImg>
                        <CardBody>
                            <CardTitle><b>
                                <Highlighter
                                highlightClassName="Active"
                                searchWords={[String(searchTerm)]}
                                autoEscape={true}
                                textToHighlight={item.event_type}
                            />
                            </b>
                            </CardTitle>
                            <CardText className="date"><b>Number of events in this type:</b> {item.num_events} <br/>
                            <b> Number of groups hosting this type:</b> {item.num_groups} <br/>
                            <b> Average group rating: </b>
                                <StarRatings
                                        rating={item.avg_group_rating}
                                        starDimension="20px"
                                        starSpacing="3px"
                                />
                            </CardText>
                            <a href={item.link + item.id}>Details</a>
                        </CardBody>

                     </Card><br/>
                     </div>

                     )}
                    </CardDeck>
                    <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage} />
                    </div>
               </div>

			);

	}
}
export default TypesOfEvents;
