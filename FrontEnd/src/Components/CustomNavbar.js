import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './CustomNavbar.css';
import { Switch } from 'react-router-dom';
import {Button} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Home from './Home.js';
import About from './About.js'; 
import AllEvents from './AllEvents.js';
import TypesOfEvents from './TypesOfEvents.js';
import CommunityGroups from './CommunityGroups.js';
import {Navbar, Nav} from 'react-bootstrap'
import {IndexLinkContainer, LinkContainer} from 'react-router-bootstrap'
import { Dropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';
import logo from "./img/logo.jpg";

class CustomNavbar extends Component {

  constructor(props) {
      super(props);

      this.state = {
        results: false,
        resultsEvents: [],
        resultsTypes: [],
        resultsGroups: [],
        term: '',
        search: false,
        dropdownOpen: false
      };

      this.submit = this.submit.bind(this);
      this.changeTerm = this.changeTerm.bind(this);
      this.toggle = this.toggle.bind(this);


      
      
  }
  
  isActivePage(url) {
    var wlp = window.location.pathname;
    if(wlp === url){
        return "active";
    }
    return "";
  }

  submit(event) {
    let url = 'https://api.volunteer4.me/search?kwd=' + encodeURI(this.state.term);
    let xhr = new XMLHttpRequest;
    var st = this
    xhr.open('GET', url, true)
    var resp = null;
    xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
                //return server response as an object with JSON.parse
                var resp = JSON.parse(this.responseText)
                st.handleEvents(resp.events)
                st.handleTypes(resp.event_types)
                st.handleGroups(resp.community_groups)
                st.handle(true)
            }
    }
    xhr.send();
    event.preventDefault()
  }

    changeTerm(event) {
      if (event.target.value == 0){
        this.setState({term: "No Results"});
      }
      else {
        this.setState({term: event.target.value});
      }
    }

    handle = (resp) => {
      this.setState( {
          results: resp
      });
    }

    handleEvents = (resp) => {
      this.setState( {
          resultsEvents: resp
      });
    }

    handleTypes = (resp) => {
      this.setState( {
          resultsTypes: resp
      });
    }

    handleGroups = (resp) => {
      this.setState( {
          resultsGroups: resp
      });
    }
    toggle() {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }
  
  

  render () {
    return(
        <div class="container">

          
        <Navbar className={this.state.navClass} fixed="top" bg="white" variant="light" expand="lg" >
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <IndexLinkContainer to="/">
            <Navbar.Brand>
                <img src="/static/media/logo.ba10053a.jpg" width="30" height="30" class="d-inline-block align-top" alt="Oh No"></img>
                  Volunteer4me
            </Navbar.Brand>
          </IndexLinkContainer>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav activeKey={1} className="ml-auto">
            <Nav.Item>
                <IndexLinkContainer to="/"><Nav.Link>Home</Nav.Link></IndexLinkContainer>
              </Nav.Item>
              <Nav.Item>
                <LinkContainer to="/AllEvents"><Nav.Link>All Events</Nav.Link></LinkContainer>
              </Nav.Item>
              <Nav.Item>
                <LinkContainer to="/TypesOfEvents"><Nav.Link>Types of Events</Nav.Link></LinkContainer>
              </Nav.Item>
              <Nav.Item>
                <LinkContainer to="/CommunityGroups"><Nav.Link>Community Groups</Nav.Link></LinkContainer>
              </Nav.Item>
              <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle nav caret id="vis">
                  Visualizations
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem href="/Vis" style={{fontSize: "90%"}}>
                    Our Site
                  </DropdownItem>
                  <DropdownItem href="/CusVis" style={{fontSize: "90%"}}>
                    Developer
                  </DropdownItem>
                </DropdownMenu>
            </Dropdown>
              <Nav.Item>
                <LinkContainer to="/About"><Nav.Link>About</Nav.Link></LinkContainer>
              </Nav.Item>
            </Nav>
              <form className="form-inline my-1 my-sm-0" onSubmit={this.submit}>
                <input className="form-control mr-sm-2" type="search" onChange={this.changeTerm} placeholder="Search all models..." aria-label="Search"></input>
                <Link to={{pathname: "/SearchedEvents/" + this.state.term, state: "true"}}>
                  <button className="btn btn-outline-success my-sm-0" type="submit" onClick={this.changeTerm}>Search</button>
                </Link>
              </form>
          </Navbar.Collapse>
        </Navbar>
          </div>
       
      );
  }
}
export default CustomNavbar;

