import React, { createRef, Component } from "react";
import * as d3 from "d3";
import axios from 'axios';


var testData = [
  {date: "label1", value: 100},
  {date: "label2", value: 100},
  {date: "label3", value: 100},
  {date: "label4", value: 100}
  // {'April' : 100},
  // {'fdsdf' : 100},
  // {'Ap234ril' : 100},
  // {'Aprfdsil' : 100}
]

var actual_data = []

class CusPieClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      data: null,
      items: null,
    };
    this.ref = createRef();
    this.createPie = d3
      .pie()
      .value(d => d.value)
      .sort(null);
    this.createArc = d3
      .arc()
      .innerRadius(props.innerRadius)
      .outerRadius(props.outerRadius);
    this.colors = d3.scaleOrdinal(d3.schemeCategory10);
    this.format = d3.format(".2f");
  }


  getMonthlyData(){
    actual_data = [];
    let full_info = {};
    var i;
    var url = 'https://api.thecharitywatch.org/charity?';
    axios.get(url).then(response => {
        // console.log(response);
        for(var i = 0; i < 9; i++) {
            var name = response.data.charities[i].charity.name;
            var income = response.data.charities[i].charity.income_total;
            if(income > 0){
            var this_dict = {date: name, value: income}
            actual_data.push(this_dict)
            }
            // var numFoods = response.data.countries[i].country.gdp_per_capita;
            // items_loc[name] = 0;
            // items_loc[name] += numFoods;
        }
        console.log(actual_data)
    //     if (month in full_info){
    //         var cur_val = full_info[month].value + 1
    //         var new_dict = {date: month, value: cur_val}
    //         full_info[month] = new_dict
    //     } else {
    //       var new_dict = {date: month, value: 1}
    //       full_info[month] = new_dict
    //     }
    //   });
    //   var other_dict = {date: "Other", value: 0}
    //   for (var key in full_info) {
    //     var inner_dict = full_info[key]
    //     if(inner_dict.value < 20){
    //         var cur_val = other_dict.value
    //         other_dict.value = cur_val + inner_dict.value
    //     }
    //   }
    //   console.log("before for loop")
    //   actual_data.push(other_dict)
    //   for (var key in full_info) {
    //     if(full_info[key].value > 20){
    //       actual_data.push(full_info[key])
    //     }
    //   }
      this.setState({isLoaded: true, data: actual_data, items: full_info});
    });
    return full_info;
  }

  componentDidMount() {
    const svg = d3.select(this.ref.current);
    this.getMonthlyData()
    const data = this.createPie(actual_data);
    const { width, height, innerRadius, outerRadius } = this.props;

    svg
      .attr("class", "chart")
      .attr("width", width)
      .attr("height", height);

    const group = svg
      .append("g")
      .attr("transform", `translate(${outerRadius} ${outerRadius})`);

    const groupWithEnter = group
      .selectAll("g.arc")
      .data(data)
      .enter();

    const path = groupWithEnter.append("g").attr("class", "arc");

    path
      .append("path")
      .attr("class", "arc")
      .attr("d", this.createArc)
      .attr("fill", (d, i) => this.colors(d.index));

    path
      .append("text")
      .attr("text-anchor", "end")
      .attr("alignment-baseline", "middle")
      .attr("transform", d => `translate(${this.createArc.centroid(d) +50})`)
      .style("fill", "white")
      .style("font-size", 10)
      .text(d => d.data.date);
  }

  componentWillUpdate(nextProps, nextState) {
    const svg = d3.select(this.ref.current);
    const data = this.createPie(actual_data);

    var getAngle = function (d) {
      return (180 / Math.PI * (d.startAngle + d.endAngle) / 2 - 90);
    };

    function angle(d) {

      console.log("in angle function")
      console.log(d)
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    }

    const group = svg
      .select("g")
      .selectAll("g.arc")
      .data(data);

    group.exit().remove();

    const groupWithUpdate = group
      .enter()
      .append("g")
      .attr("class", "arc");

    const path = groupWithUpdate.append("path").merge(group.select("path.arc"));

    path
      .attr("class", "arc")
      .attr("d", this.createArc)
      .attr("fill", (d, i) => this.colors(i));

    const text = groupWithUpdate.append("text").merge(group.select("text"));

    text
      .attr("text-anchor", "middle")
      .attr("dy", 5)

      .attr("alignment-baseline", "end")
      .attr("transform", d => {
        return "translate(" + `${this.createArc.centroid(d)} ` + ")rotate(" + angle(d) + ")";
      })
      .text(d => d.data.date);

  }

  render() {
    return <svg ref={this.ref} />;
  }
}

export default CusPieClass;