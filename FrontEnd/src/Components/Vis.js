import React, { Component } from 'react';
import MapVisual from './UsaMap';
import Bubble from './Bubble';
import PieChart from './Piechart';
import BarGraph from './BarGraph';


import './Vis.css';

class Vis extends Component {
  render() {
    return (
      <div className="container mb-5">
        <div id="MapVisual">
          <h1 id="vis-header">Events per State</h1>
          <MapVisual/>
        </div>
        <div id="BubbleVisual">
          <h1 id="vis-header">Event Type Statistics</h1>
          <Bubble/>
        </div>
        <div id="PieVisual">
          <h1 id="vis-header">Monthly Breakdown of Events</h1>
          <div className="pie">
            <PieChart
              width={800}
              height={800}
              innerRadius={200}
              outerRadius={400}
            />
        </div>
        </div>
      </div>
    );
  }
}

export default Vis;
