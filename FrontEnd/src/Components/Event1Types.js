import React, { Component } from 'react';
import Pagination from './Pagination';
import YouTube from 'react-youtube';

import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, CardHeader, CardFooter} from 'reactstrap';
import dogVolunteer from './img/dogVolunteer.jpg'

const styles = {
  root: {
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "rows",
    flexFlow: "row wrap",
    justifyContent: "center",
    padding: "3vh",
  }
};

class Event1Types extends Component{

    constructor(props) {
        super(props);

        this.state = {
            pageOfItems: [],
            response: {},
            exampleItems: {},
            com_groups: []
        };

        this.onChangePage = this.onChangePage.bind(this);
    }

    exampItems = (resp) => {
        this.setState( {
            exampleItems: resp
        });
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    componentDidMount() {
        var id = this.props.match.params.EventTypeNum
        let xhr = new XMLHttpRequest;
        var url = 'https://api.volunteer4.me/eventTypes?type_id=' + id
        xhr.open('GET', url, true)
        var st = this
        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var list_of_keys = []
              var resp2 = resp[id]["related_events"]
              var length = Object.keys(resp2).length;
              var exampleItems = [...Array(length).keys()].map(i => ({
              img: resp2[i + 1]["photo_url"],
              event_type: resp2[i+ 1]["event_type"],
              title: resp2[i + 1]["event_name"],
              zipcode: resp2[i + 1]["zipcode"],
              time: resp2[i + 1]["time"],
              com_group: resp2[i + 1]["com_group"],
              video_id: resp2[i + 1]["video_id"],
              id: resp2[i + 1]["id"],
              link: "Event/"}));
              st.exampItems(exampleItems)
              st.group_handle(resp[id]["related_com_groups"])
            }
        }
        xhr.send();
    }

    handle = (resp) => {
        this.setState( {
            response: resp
        });
    }

    group_handle = (r) => {
        this.setState( {
            com_groups: r
        });
    }

    render() {
        var id = this.props.match.params.EventTypeNum
        var resp = this.state.response
        if (resp[id] != undefined) {
          var photo_url = resp[id]["photo_url"]
          var event_type_name = resp[id]["event_type_name"]
          var num_events = resp[id]["num_events"]
          var description = {__html: resp[id]["description"]}
          var video_id = resp[id]["video_id"]
        }

        const opts = {
            height: '390',
            width: '640',
            playerVars: { // https://developers.google.com/youtube/player_parameters
              autoplay: 1
            }
        };


      return(
        <div className="root" style={styles.root}>
        <div class="container" style={styles.root}>
          <Card raised className="title-card">
          <CardHeader className="cardHeader" tag="h3">{event_type_name}</CardHeader>
          <CardBody>
            <Row className="top">
              <Col className="leftSide">
                <CardImg className="photo" src={photo_url}></CardImg>
                <br></br><br></br>
                <CardTitle tag="h5" className="numEvents">Number of Events in this Group: {num_events}</CardTitle>
              </Col>
              <Col>
              <YouTube
                  videoId={video_id}
                  opts={opts}
                  onReady={this._onReady}/>
              </Col>
            </Row>
            <CardTitle tag="h5">Description:</CardTitle>
            <CardText><div dangerouslySetInnerHTML={description}/></CardText>
          </CardBody>
          <CardFooter>
          <Row>
            <h3>Related Events:</h3>
              <div className="container">
                <CardDeck>
                {this.state.pageOfItems.map(item =>
                    <div class="col-4">
                        <Card raised className="main-card" style={{ height: '30rem' }}>
                            <CardImg src={item.img.replace('global','highres')}></CardImg>
                            <CardBody>
                                <CardTitle ><b>Title: {item.title}</b></CardTitle>
                                <CardText className="date"><b>Date:</b> {item.time} <br/>
                                <b>Zipcode:</b> {item.zipcode} <br/> <b>Community Group:</b> {item.com_group}
                                <br/> <b>Type of Event:</b> {item.event_type}</CardText>
                                <a base href={"http://" + window.location.host + "/Event/" + item.id}>Details</a>
                            </CardBody>
                        </Card><br/>
                    </div>
                  )}
                </CardDeck>
                <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage} />
              </div>
          </Row>
          <Row>
            <h3>Related Community Groups:</h3>
            <div className="container">
            {this.state.com_groups.map(item =>
                <li><a base href={"http://" + window.location.host + "/CommunityGroupsInstance/" + item.id}><b>{item.name}</b></a></li>
            )}
            </div>
          </Row>
          </CardFooter>
          </Card>
        </div>
        </div>);
    }
}

export default Event1Types;
