import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'
import pic1 from './img/pic1.jpg';
import pic2 from './img/pic2.jpg';
import pic3 from './img/pic3.jpg';


class CarouCard extends Component{
  constructor(props, context) {
    super(props, context);

    this.state = {
      index: 0,
      direction: null
    };

    this.handleSelect = this.handleSelect.bind(this);

  }

  handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }

	render() {
    const { index, direction } = this.state;
		return(
  			<Carousel
          activeIndex={index}
          direction={direction}
          onSelect={this.handleSelect}
        >
          <Carousel.Item>
            <img className="d-block w-100 h-75" src={pic1} />
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block w-100 h-75" src={pic2} />
          </Carousel.Item>
          <Carousel.Item>
              <img className="d-block w-100 h-75" src={pic3} />
          </Carousel.Item> 

        </Carousel>
    );
	}
}

export default CarouCard;
