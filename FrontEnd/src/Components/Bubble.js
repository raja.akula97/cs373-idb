import React, { Component } from 'react';
import * as d3 from "d3";
import BubbleChart from '@weknow/react-bubble-chart-d3';
import axios from 'axios';

import './Vis.css';

class Bubble extends Component {

constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      data: null,
      items: null,
    };
  }

  getEventTypeData(){
    let eventtypes = [];
    let full_info = {};
    axios.get('https://api.volunteer4.me/eventTypes?all=True').then(response => {
      response.data.forEach(event => {
      const num_events = event.num_events;
      const name = event.event_type_name;
      const desc = event.description;
      const num_com_groups = event.num_groups;
      const id = event.id;
      var disasterData = {label: name, value: num_events};
      full_info[name] = {desc: desc, com_groups: num_com_groups, id: id};
      eventtypes.push(disasterData);
    });
    this.setState({isLoaded: true, data: eventtypes, items: full_info});
    });

    return eventtypes;
  }

  componentDidMount() {
    this.getEventTypeData();
  }

  render() {
    if(this.state.isLoaded){
      var data = this.state.data;
    }
    var bubbleClick = (label) =>{
      document.getElementById("title").innerHTML = label;
      var data = this.state.items[label];
      document.getElementById("desc").innerHTML = "Short Description: " + data['desc'] + " ...";
      document.getElementById("num_com_groups").innerHTML = "Number of Community Groups: " + data['com_groups'];
      document.getElementById("link").href = "/Event1Types/" + data['id'];
    }
    return (
      <div className="container mb-3">

        <div className="row">

          <div className="card col-lg-3" style={{height: "100%", marginTop: "20vh"}}>
            <div className="card-body">
              <h5 className="card-title" id="title">Click on a Bubble for more information</h5>
              <ul className="list-group list-group-flush">
                <li className="list-group-item" id="desc">Short Description: </li>
                <li className="list-group-item" id="num_com_groups">Number of Community Groups: </li>
              </ul>
              <a href="#BubbleVisual" id="link" className="card-link">Learn More</a>
            </div>
          </div>

          <div className="col-lg-9">
            <BubbleChart id="bubbleChart"
              graph= {{
                zoom: .65,
                offsetX: 0.15,
                offsetY: 0.05,
              }}
              showLegend={false} // optional value, pass false to disable the legend.
              legendPercentage={20} // number that represent the % of with that legend going to use.
              legendFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#000',
                    weight: 'bold',
                  }}
              valueFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#fff',
                    weight: 'bold',
                  }}
              labelFont={{
                    family: 'Arial',
                    size: 16,
                    color: '#fff',
                    weight: 'normal',
                  }}
              data={data}
              bubbleClickFun={bubbleClick}
            />
          </div>

        </div>
      </div>
    );
  }
}
export default Bubble;
