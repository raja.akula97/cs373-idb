import React, { Component } from 'react';
import SearchField from "react-search-field";
import DateRangePicker from 'react-daterange-picker';
import 'react-daterange-picker/dist/css/react-calendar.css';
import { DateRange } from 'react-date-range';
import Pagination from './Pagination';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, ButtonToolbar} from 'reactstrap';
import Event from './Event.js';
import './AllEvents.css';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';
import 'react-daterange-picker/dist/css/react-calendar.css';
import Button2 from 'react-bootstrap/Button';
import Highlighter from "react-highlight-words";
import Select from 'react-select'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

var prevSearch = "";
var prevZip = "";


var globalURL = "https://api.volunteer4.me/event?all=True"
var globalEvent = ""

var dictURL = {
  zipcode: "",
  event_type: "",
  sort: "",
  endDate: "",
  startDate: "",
  search: ""
};

const ratings = [
    { value: '4.9', label: '> 4.9' },
    { value: '4.0', label: '> 4.0' },
    { value: '3.0', label: '> 3.0' },
    { value: '2.0', label: '> 2.0' },
    { value: '1.0', label: '> 1.0' }
  ]

const sorts = [
    { value: 'event true', label: 'Event name: A-Z' },
    { value: 'event false', label: 'Event name: Z-A' },
    { value: 'cg true', label: 'Community Group name: A-Z'},
    { value: 'cg false', label: 'Community Group name: Z-A'},
    { value: 'date true', label: "Date: Earliest to Latest"},
    { value: 'date false', label: 'Date: Latest to Earliest'}
  ]

var endDate = new Date()

var searchTerm = "";

const event_types = [
    { value: "Arts & Culture", label: "Arts & Culture"},
    { value: "Games", label: "Games"},
    { value: "Book Clubs", label: "Book Clubs"},
    { value: "Socializing", label: "Socializing"},
    { value: "Career & Business", label: "Career & Business"},
    { value: "Education & Learning", label: "Education & Learning"},
    { value: "Cars & Motorcycles", label: "Cars & Motorcycles"},
    { value: "Community & Environment", label: "Community & Environment"},
    { value: "Support", label: "Support"},
    { value: "Tech", label: "Tech"},
    { value: "Pets & Animals", label: "Pets & Animals"},
    { value: "Dancing", label: "Dancing"},
    { value: "Fitness", label: "Fitness"},
    { value: "Religion & Beliefs", label: "Religion & Beliefs"},
    { value: "Fashion & Beauty", label: "Fashion & Beauty"},
    { value: "Food & Drink", label: "Food & Drink"},
    { value: "Health & Wellbeing", label: "Health & Wellbeing"},
    { value: "Language & Ethnic Identity", label: "Language & Ethnic Identity"},
    { value: "Movements & Politics", label: "Movements & Politics"},
    { value: "Hobbies & Crafts", label: "Hobbies & Crafts"},
    { value: "Movies & Film", label: "Movies & Film"},
    { value: "Music", label: "Music"},
    { value: "New Age & Spirituality", label: "New Age & Spirituality"},
    { value: "Outdoors & Adventure", label: "Outdoors & Adventure"},
    { value: "Parents & Family", label: "Parents & Family"},
    { value: "Paranormal", label: "Paranormal"},
    { value: "Photography", label: "Photography"},
    { value: "Sports & Recreation", label: "Sports & Recreation"},
    { value: "Sci-Fi & Fantasy", label: "Sci-Fi & Fantasy"},
    { value: "Writing", label: "Writing"}
]


class AllEvents extends React.Component{
     constructor() {
        super();

        this.state = {
            startDate: new Date(),
            pageOfItems: [],
            response: null,
            event_name: [],
            exampleItems: {},
            exampleStr : "blah",
            query: '',
            dates: null,
            eventTypesArr: []

        };

        // bind function in constructor instead of render (https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)
        this.onChangePage = this.onChangePage.bind(this);
        this.nameSort = this.nameSort.bind(this);
        this.eventTypeFilter = this.eventTypeFilter.bind(this);
        this.zipcodeFilter = this.zipcodeFilter.bind(this);
        this.searchFilter = this.searchFilter.bind(this);
        this.filtering = this.filtering.bind(this);
        this.initialRender = this.initialRender.bind(this);
        this.handleChangeStart = this.handleChangeStart.bind(this);

    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handle(resp) {
        this.setState( {
            response: resp
        });
    }

    date_handle(d) {
        this.setState( {
            startDate: d
        });
    }

    exampItems(resp) {
        this.setState( {
            exampleItems: resp
        });
    }

    eventTypesArr(resp) {
        this.setState( {
            eventTypesArr: resp
        });
    }

    initialRender() {
        window.location.reload()

    }

    componentDidMount() {
        dictURL["zipcode"] = '';
        dictURL["sort"] = '';
        dictURL["endDate"] = '';
        dictURL["startDate"] = '';
        dictURL["event_type"] = '';
        dictURL["search"] = '';
        let xhr = new XMLHttpRequest;
        var st = this
        xhr.open('GET', 'https://api.volunteer4.me/event?all=True', true)
        var resp = null;

        var bar = document.getElementById("search_bar");


        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
                //return server response as an object with JSON.parse
                var resp = JSON.parse(this.responseText)

                st.handle(resp)

                var length = resp.length;

                var exampleItems = [...Array(length).keys()].map(i => ({
                id: resp[i].id,
                img: resp[i].photo_url,
                event_type: resp[i].event_type,
                title: resp[i].event_name,
                zipcode: resp[i].zipcode,
                time: resp[i].time,
                com_group: resp[i].com_group,
                id: resp[i].id,
                link: "Event/"}));
                st.exampItems(exampleItems)

                var eventTypes = [...Array(length).keys()].map(i => ({
                    event_type: resp[i].event_type,
                }));
                var tempeventTypesArr = []
                var count = 0
                for(var val in eventTypes) {
                    if(!tempeventTypesArr.includes(eventTypes[val]["event_type"])) {
                        tempeventTypesArr.push(eventTypes[val]["event_type"])
                    }
                }
                st.eventTypesArr(tempeventTypesArr)
            }
        }
        xhr.send();
    }

    nameSort(ascending){
        let xhr = new XMLHttpRequest;
        var st = this
        var val = true
        var sortKind = ascending['value'].split(' ')
        var resultString = '&sortby='
        if (sortKind[0] == 'event'){
            resultString+='event_name'
        }
        else if (sortKind[0] == 'cg'){
            resultString+='com_group'
        }
        else if (sortKind[0] == 'date'){
            resultString+='time'
        }
        if (sortKind[1] == 'false'){
            resultString += '&sortrev=True'
        }
        dictURL["sort"] = resultString
        st.filtering(xhr, st)
    }

    eventTypeFilter(type) {
       var encoded_uri = encodeURIComponent(type['value'])
       let xhr = new XMLHttpRequest;
       dictURL["event_type"] = '&event_type=' +  encoded_uri
       var st = this

       st.filtering(xhr, st)
    }

    zipcodeFilter(zipcode) {
        if(this.search2.value.length == 5) {
            prevZip = this.search2.value
            var encoded_uri = encodeURIComponent(this.search2.value)
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["zipcode"] = '&zipcode=' +  encoded_uri

            st.filtering(xhr, st)
        } else if (prevZip.length > 0 && this.search2.value.length == 0) {
            prevZip = this.search2.value
            window.location.reload()
        }
    }

    searchFilter(keywordVal) {
        if(prevSearch.length > 0 && this.search5.value.length == 0) {
            prevSearch = this.search5.value
            window.location.reload()
        } else {
            prevSearch = this.search5.value
            var encoded_uri = encodeURIComponent(this.search5.value)
            let xhr = new XMLHttpRequest;
            var st = this

            dictURL["search"] = '&search=' +  encoded_uri
            searchTerm = encoded_uri;

            st.filtering(xhr, st)
        }
    }


    handleChangeStart(d) {
        this.date_handle(d)
        var st = this
        var month1 = d.getMonth() + 1
        var year1 = d.getFullYear()
        var date1 = d.getDate()

        dictURL["startDate"] = '&date_b=' + year1 + '-' + month1 + "-" + date1
        dictURL["endDate"] = '&date_e=' + year1 + '-' + month1 + "-" + date1

        let xhr = new XMLHttpRequest;
        st.filtering(xhr, st)
    }



    filtering(xhr, st) {
        var url = globalURL + dictURL["sort"] + dictURL["event_type"] + dictURL["zipcode"] + dictURL["startDate"] + dictURL["endDate"]
        + dictURL["search"]
        xhr.open('GET', url, true)
        var resp = null;

        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
                //return server response as an object with JSON.parse
                resp = JSON.parse(this.responseText)
                st.handle(resp)
                var length = resp.length;
                var exampleItems = [...Array(length).keys()].map(i => ({
                id: resp[i].id,
                img: resp[i].photo_url,
                event_type: resp[i].event_type,
                title: resp[i].event_name,
                zipcode: resp[i].zipcode,
                time: resp[i].time,
                com_group: resp[i].com_group,
                link: "Event/"}));
                st.exampItems(exampleItems)
            }
        }
        xhr.send();
    }



    render() {
        const selectionRange = {
            startDate: new Date(),
            endDate: new Date(),
            key: 'selection',
        }
        return(
            <div className="root">
              <br></br>
              <br></br>
              <br></br>
                <div class="container">
                  <div class="row">
                    <div class="col-sm">
                      <Select placeholder="Event Type" options={event_types} onChange={this.eventTypeFilter}/>
                    </div>
                  <div class="col-sm">
                      <input className="form-control mr-sm-2" onChange={this.zipcodeFilter} placeholder="Search zipcode..." ref={input => this.search2 = input}/>
                  </div>
                  <div class="col-sm">
                    <DatePicker className='datePicker'
                                selectsStart
                                startDate={this.state.startDate}
                                endDate={endDate}
                                selected={this.state.startDate}
                                dateFormat="MM-dd-yyyy"
                                onChange={this.handleChangeStart}/>
                  </div>
                  <div class="col-sm">
                    <Select isClearable={true} placeholder="Sort" options={sorts} onChange={this.nameSort}/>
                  </div>
                  <div class="col-sm">
                    <input className="form-control mr-sm-2" onChange={this.searchFilter} placeholder="Search events..." ref={input => this.search5 = input}/>
                  </div>
                  <div class="col-md-auto">
                    <Button2 variant="outline-danger" onClick={this.initialRender}>Reset Filters</Button2>
                  </div>
                </div>
                <p></p>
                <CardDeck>
                  {this.state.pageOfItems.map(item =>
                    <div class="col-4">
                      <Card raised className="main-card">
                      <CardImg height="190px" src={item.img.replace('global','highres')} ></CardImg>
                      <CardBody>
                      <CardTitle ><b>
                      <Highlighter  highlightClassName="Active"
                                    searchWords={[String(searchTerm)]}
                                    autoEscape={true}
                                    textToHighlight={item.title}/></b>
                      </CardTitle>
                      <CardText className="date"><b>Date:</b> {item.time} <br/>
                      <b>Zipcode: </b> <Highlighter  highlightClassName="Active"
                                    searchWords={[String(searchTerm)]}
                                    autoEscape={true}
                                    textToHighlight={item.zipcode}/><br/>
                      <b>Community Group: </b>
                      <Highlighter  highlightClassName="Active"
                                    searchWords={[String(searchTerm)]}
                                    autoEscape={true}
                                    textToHighlight={item.com_group}/><br/>
                      <b>Type of Event: </b>
                      <Highlighter  highlightClassName="Active"
                                    searchWords={[String(searchTerm)]}
                                    autoEscape={true}
                                    textToHighlight={item.event_type}/></CardText>
                      <div className="bottom">
                        <a href={item.link + item.id}>Details</a>
                      </div>
                      </CardBody>
                      </Card><br/>
                    </div>)}
                  </CardDeck>
                  <div className="page">
                    <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage}/></div>
                  </div>
               </div>
            );
    }
}

export default AllEvents;
