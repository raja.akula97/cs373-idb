import React, { Component } from 'react';
import * as d3 from "d3";
import BubbleChart from '@weknow/react-bubble-chart-d3';
import axios from 'axios';

import './Vis.css';

class Bubble extends Component {

constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      data: null,
      items: null,
    };
  }

  getEventTypeData(){
    let eventtypes = [];
    let full_info = {};
    axios.get('https://api.thecharitywatch.org/person?orderby=compensation&desc=true&start=1&limit=12').then(response => {
        console.log(response.data.people)
        for(var i = 0; i < 12; i++) {
          var name = response.data.people[i].person.name
          var income = response.data.people[i].person.total_compensation
          var desc = response.data.people[i].person.main_role
          var id =  response.data.people[i].person.id
          var hours = response.data.people[i].person.total_avg_hours
          console.log(hours)
          if(income > 0){
            var this_dict = {label: name, value: income};
            full_info[name] = {desc: desc, com_groups: hours, id: id};
            eventtypes.push(this_dict)
          }
        }
    this.setState({isLoaded: true, data: eventtypes, items: full_info});
    });

    return eventtypes;
  }

  componentDidMount() {
    this.getEventTypeData();
  }

  render() {
    if(this.state.isLoaded){
      var data = this.state.data;
    }
    var bubbleClick = (label) =>{
      document.getElementById("title").innerHTML = label;
      var data = this.state.items[label];
      document.getElementById("desc").innerHTML = "Main Role: " + data['desc'];
      document.getElementById("num_com_groups").innerHTML = "Total Hours per week: " + data['com_groups'];
      // document.getElementById("link").href = "/Event1Types/" + data['id'];
    }
    return (
      <div className="container mb-3">

        <div className="row">

          <div className="card col-lg-3" style={{height: "100%", marginTop: "20vh"}}>
            <div className="card-body">
              <h5 className="card-title" id="title">Click on a Bubble for more information</h5>
              <ul className="list-group list-group-flush">
                <li className="list-group-item" id="desc">Main Role: </li>
                <li className="list-group-item" id="num_com_groups">Total Hours per week: </li>
              </ul>
            </div>
          </div>

          <div className="col-lg-9">
            <BubbleChart id="bubbleChart"
              graph= {{
                zoom: .65,
                offsetX: 0.15,
                offsetY: 0.05,
              }}
              showLegend={false} // optional value, pass false to disable the legend.
              legendPercentage={20} // number that represent the % of with that legend going to use.
              legendFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#000',
                    weight: 'bold',
                  }}
              valueFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#fff',
                    weight: 'bold',
                  }}
              labelFont={{
                    family: 'Arial',
                    size: 16,
                    color: '#fff',
                    weight: 'normal',
                  }}
              data={data}
              bubbleClickFun={bubbleClick}
            />
          </div>

        </div>
      </div>
    );
  }
}
export default Bubble;
