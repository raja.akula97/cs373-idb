import React, { Component } from 'react';
import * as d3 from 'd3';
import axios from 'axios';
import './BarGraph.css';

//Bar graph visualization for UnbEATable Food
class BarGraph extends Component {

  constructor(props) {
	    super(props);
	    this.state = {
	      isLoaded: false,
	      items: {}
	    };
      this.create = this.create.bind(this);
      this.getLocationData = this.getLocationData.bind(this);
	}

	create () {
		const sample = [
		  {
		    language: '  ASM',
		    value: this.state.items["ASM"]
		  },
		  {
		    language: '  ARM',
		    value: this.state.items["ARM"]
		  },
		  {
		    language: '  BHR',
		    value: this.state.items["BHR"]
		  },
		  {
		    language: '  BMU',
		    value: this.state.items["BMU"]
		  },
		  {
		    language: '  BIH',
		    value: this.state.items["BIH"]
		  },
		  {
		    language: '  BGR',
		    value: this.state.items["BGR"]
		  },
		  {
		    language: '  KHM',
		    value: this.state.items["KHM"]
		  },
		  {
		    language: '  TCD',
		    value: this.state.items["TCD"]
		  },
		  {
		    language: "  COM",
		    value: this.state.items["COM"]
		  }
		];

	    const svg = d3.select('#bar-graph-loc');
	    const svgContainer = d3.select('#container-loc');

	    const margin = 80;
	    const width = 1000 - 2 * margin;
	    const height = 600 - 2 * margin;

	    const chart = svg.append('g')
	      .attr('transform', `translate(${margin}, ${margin})`);

	    const xScale = d3.scaleBand()
	      .range([0, width])
	      .domain(sample.map((s) => s.language))
	      .padding(0.4)

	    const yScale = d3.scaleLinear()
	      .range([height, 0])
	      .domain([0, 100000]);

	    const makeYLines = () => d3.axisLeft()
	      .scale(yScale)

	    chart.append('g')
	      .attr('transform', `translate(0, ${height})`)
	      .call(d3.axisBottom(xScale))
        .selectAll('text')
          .attr("transform", "rotate(90)")
          .style("text-anchor", "start")
          .style("fill", "white")
          .style("font-size", "15px");

	    chart.append('g')
	      .call(d3.axisLeft(yScale))
        .selectAll('text')
          .style("fill", "white")
          .style("font-size", "15px");

	    chart.append('g')
	      .attr('class', 'grid')
	      .call(makeYLines()
	        .tickSize(-width, 0, 0)
	        .tickFormat('')
	      )

	    const barGroups = chart.selectAll()
	      .data(sample)
	      .enter()
	      .append('g')

	    barGroups
	      .append('rect')
	      .attr('class', 'bar')
	      .attr('x', (g) => xScale(g.language))
	      .attr('y', (g) => yScale(g.value))
	      .attr('height', (g) => height - yScale(g.value))
	      .attr('width', xScale.bandwidth())

	    barGroups
	      .append('text')
	      .attr('class', 'value')
	      .attr('x', (a) => xScale(a.language) + xScale.bandwidth() / 2)
	      .attr('y', (a) => yScale(a.value) + 40)
	      .attr('text-anchor', 'middle')

	    svg
	      .append('text')
	      .attr('class', 'label')
	      .attr('x', -(height / 2) - margin)
	      .attr('y', margin / 4.2)
	      .attr('transform', 'rotate(-90)')
	      .attr('text-anchor', 'middle')
	      .text('GDP Per Capita\n')
          .style("font-size", "20px");
	}

  getLocationData() {
    let items_loc = {};
      axios.get("https://api.thecharitywatch.org/country").then(response => {
        // console.log(response);
        for(var i = 0; i < 9; i++) {
            var name = response.data.countries[i].country.abbr
            var numFoods = response.data.countries[i].country.gdp_per_capita;
            items_loc[name] = 0;
            items_loc[name] += numFoods;
        }
        this.forceUpdate();
      });
    this.setState({isLoaded: true, items: items_loc});
  }

  componentDidUpdate() {
    this.create();
  }

  componentDidMount() {
    this.getLocationData();
  }

  render() {
    return (
      <div id='container-loc'>
          <svg id="bar-graph-loc"></svg>
      </div>
    );
  }

}

export default BarGraph;
