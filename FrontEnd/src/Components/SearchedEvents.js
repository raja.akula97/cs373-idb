import React, { Component } from 'react';
import Pagination from './Pagination';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, ButtonToolbar} from 'reactstrap';
import Highlighter from "react-highlight-words";
import './SearchedEvents.css';

var global_url = 'https://api.volunteer4.me/search?kwd='
class SearchedEvents extends Component {

  constructor(props) {
        super(props);

        this.state={
          pageOfItemsEvents: [],
          pageOfItemsTypes: [],
          pageOfItemsGroups: [],
          responseEvents: [],
          responseTypes: [],
          responseGroups: [],



          exampleItemsEvents: {},
          exampleItemsTypes: {},
          exampleItemsGroups: {},
          
          results: false,
          searchTerm: "",
          resultsEvents: {},
          resultsTypes: {},
          resultsGroups: {},
        }

        //window.location.reload()

        this.onChangePageEvents = this.onChangePageEvents.bind(this);
        this.onChangePageGroups = this.onChangePageGroups.bind(this);
        this.onChangePageTypes = this.onChangePageTypes.bind(this);

  }

  onChangePageEvents(pageOfItemsEvents) {
      // update state with new page of items
      this.setState({ pageOfItemsEvents: pageOfItemsEvents });
  }

  onChangePageTypes(pageOfItemsTypes) {
      // update state with new page of items
      this.setState({ pageOfItemsTypes: pageOfItemsTypes });
  }

  onChangePageGroups(pageOfItemsGroups) {
      // update state with new page of items
      this.setState({ pageOfItemsGroups: pageOfItemsGroups });
  }

  exampItemsEvents = (resp) => {
        this.setState( {
            exampleItemsEvents: resp
        });
    }

  exampItemsTypes = (resp) => {
        this.setState( {
            exampleItemsTypes: resp
        });
    }

  exampleItemsGroups = (resp) => {
        this.setState( {
            exampleItemsGroups: resp
        });
    }

  handleTerm = (resp) => {
    this.setState( {
        searchTerm: resp
    });
  }

  componentDidMount() {
    var term = this.props.match.params.searchTerm
    this.handleTerm(term)    
    let url = global_url
    global_url += term
    let xhr = new XMLHttpRequest;
    var st = this
    xhr.open('GET', global_url, true)
    var resp = null;

    xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
                //return server response as an object with JSON.parse
                var resp = JSON.parse(this.responseText)
                var respEvents = resp.events
                var exampleItemsEvents = [...Array(respEvents.length).keys()].map(i => ({
                    id: respEvents[i].id,
                    img: respEvents[i].photo_url,
                    event_type: respEvents[i].event_type,
                    title: respEvents[i].event_name,
                    zipcode: respEvents[i].zipcode,
                    time: respEvents[i].time,
                    com_group: respEvents[i].com_group,
                    id: respEvents[i].id,
                    link: "/Event/"}));
                st.exampItemsEvents(exampleItemsEvents)
                

                var respTypes = resp.event_types

                var exampleItemsTypes = [...Array(respTypes.length).keys()].map(i => ({ 
                    num_events: respTypes[i]['num_events'],
                    num_groups: respTypes[i]["num_groups"],
                    avg_group_rating: Math.round((respTypes[i]["avg_group_rating"] * 100))/100,
                    event_type: respTypes[i]["event_type_name"],
                    description: respTypes[i]["description"],
                    photo_url: respTypes[i]["photo_url"],
                    id: respTypes[i]["id"],
                    link: "/Event1Types/"}));
                st.exampItemsTypes(exampleItemsTypes)


                var respGroups = resp.community_groups
                var exampleItemsGroups = [...Array(respGroups.length).keys()].map(i => ({ 
                    img: respGroups[i]["photo_url"],
                    com_group: respGroups[i]["community_group_name"],
                    event_count: respGroups[i]["num_events"],
                    rating: respGroups[i]["rating"],
                    member_count: respGroups[i]["Member Count"],
                    description: respGroups[i]["description"],
                    id: respGroups[i]["id"],
                    link: "/CommunityGroupsInstance/"}));
                st.exampleItemsGroups(exampleItemsGroups)

            }
    }
    xhr.send();
    global_url = url + ""
  }
  

  render() {
     if(this.props.location.state) {
      this.componentDidMount()
    }
    this.props.location.state = false
  return (
      <div class="root">
        <div className="container">
        <br></br>
        <br></br>
        <br></br>
        <h2>SEARCH RESULTS</h2>

        <h3> All Events matching the search: </h3>
        <p></p>
        <CardDeck>
            {this.state.pageOfItemsEvents.map(item =>
                <div class="col-4">
                    <Card raised className="main-card">
                    <CardImg src={item.img} height = "200px"></CardImg>
                    <CardBody>
                        <CardTitle ><b>Title: 
                          <Highlighter
                                  highlightClassName="Active"
                                  searchWords={[String(this.state.searchTerm)]}
                                  autoEscape={true}
                                  textToHighlight={item.title}
                          /></b>
                        </CardTitle>
                        <CardText className="date"><b>Date:</b> {item.time} <br/>
                        <b>Zipcode:</b> <Highlighter
                                  highlightClassName="Active"
                                  searchWords={[String(this.state.searchTerm)]}
                                  autoEscape={true}
                                  textToHighlight={item.zipcode}
                          />
                           <br/> <b>Community Group:</b> <Highlighter
                                  highlightClassName="Active"
                                  searchWords={[String(this.state.searchTerm)]}
                                  autoEscape={true}
                                  textToHighlight={item.com_group}
                          />
                        <br/> <b>Type of Event:</b> {item.event_type}</CardText>
                        <div className="bottom">
                            <a href={item.link + item.id}>Details</a>
                        </div>
                    </CardBody>
                    </Card><br/>
                </div>
            )}
        </CardDeck>

        <div className="page">
        <Pagination items={this.state.exampleItemsEvents} onChangePage={this.onChangePageEvents} forcePage={this.state.selected} pageSize={3}/>
        </div>
        <p></p>

        <p></p>
        <h3> Types of Events matching the search: </h3>
        <p></p>
        <CardDeck>
                    {this.state.pageOfItemsTypes.map(item =>
                     <div class="col-4">
                     <Card raised className="main-card">
                        <CardImg src={item.photo_url}></CardImg>
                        <CardBody>
                            <CardTitle><b>Title: 
                              <Highlighter
                                highlightClassName="Active"
                                searchWords={[String(this.state.searchTerm)]}
                                autoEscape={true}
                                textToHighlight={item.event_type}
                              /></b>
                            </CardTitle>
                            <CardText className="date"><b>Number of events in this type:</b> {item.num_events} <br/>
                            <b> Number of groups hosting this type:</b> {item.num_groups} <br/>
                            <b> Average group rating: </b> {item.avg_group_rating} </CardText>
                            <a href={item.link + item.id}>Details</a>
                        </CardBody>

                     </Card><br/>
                     </div>

                     )}
                    </CardDeck>

        
        <div className="page">
        <Pagination items={this.state.exampleItemsTypes} onChangePage={this.onChangePageTypes} forcePage={this.state.selected} pageSize={3}/>
        </div>
        <p></p>

        <p></p>
        <h3> Community Groups matching the search: </h3>
        <p></p>
        <CardDeck>
                    {this.state.pageOfItemsGroups.map(item =>
                     <div class="col-4">
                     <Card raised className="main-card">
                     <CardImg src={item.img} height="200px"></CardImg>
                     <CardBody>
                        <CardTitle><b>Community Group: 
                          <Highlighter
                              highlightClassName="Active"
                              searchWords={[String(this.state.searchTerm)]}
                              autoEscape={true}
                              textToHighlight={item.com_group}
                          /></b>
                        </CardTitle>
                            <CardText className="date"><b>Group Rating:</b> {item.rating}<br/><b>
                            Number of Events in the Group:</b> {item.event_count}
                            <br/> <b>Member Count:</b> {item.member_count} </CardText>
                            <a href={item.link + item.id}>Details</a>
                        </CardBody>

                     </Card><br/>
                     </div>

                     )}
          </CardDeck>

        

        
        <div className="page">
        <Pagination items={this.state.exampleItemsGroups} onChangePage={this.onChangePageGroups} forcePage={this.state.selected} pageSize={3}/>
        </div>

        </div>
        </div>
        

    );
  }

}

export default SearchedEvents;
