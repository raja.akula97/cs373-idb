import React, { Component } from 'react';
import PieChart from './CusPiechart';
import BarGraph from './BarGraph';
import Bubble from './CusBubble';


import './Vis.css';

class CusVis extends Component {
  render() {
    return (
      <div className="container mb-5">
        <div id="bargraph">
          <h1 id="vis-header">Countries by GDP per Capita</h1>
          <BarGraph/>
        </div>
        <div className="container mb-5">
            <div id="bargraph">
            <h1 id="vis-header">Highest Earners</h1>
            <Bubble/>
            </div>
        </div>
        <div id="PieVisual">
          <h1 id="vis-header">Charity Total income</h1>
          <h5>Charities with 0 income not listed</h5>
          <div className="pie">
            <PieChart
                width={1200}
                height={800}
                innerRadius={0}
                outerRadius={400}
            />
            </div>
        </div>
        
      </div>
    );
  }
}

export default CusVis;
