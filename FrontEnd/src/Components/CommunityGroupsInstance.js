import React, { Component } from 'react';
import Pagination from './Pagination';

import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, CardHeader, CardFooter} from 'reactstrap';
import Event from'./Event.js'
import StarRatings from 'react-star-ratings';
import './CommunityGroupsInstance.css'

const styles = {
  root: {
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "rows",
    flexFlow: "row wrap",
    justifyContent: "center",
    padding: "3vh",
  }
};

class CommunityGroupsInstance extends Component{

    constructor(props) {
        super(props);
        this.state = {
            pageOfItems: [],
            response: {},
            exampleItems: {},
            event_types: []
        };

        this.onChangePage = this.onChangePage.bind(this);
    }

    exampItems = (resp) => {
        this.setState( {
            exampleItems: resp
        });
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    componentDidMount() {
        var id = this.props.match.params.GroupId
        let xhr = new XMLHttpRequest;
        var url = 'https://api.volunteer4.me/communityGroups?group_id=' + id
        xhr.open('GET', url, true)
        var st = this
        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var resp2 = resp[id]["related_events"]
              var length = Object.keys(resp2).length;
              var exampleItems = [...Array(length).keys()].map(i => ({
              img: resp2[i + 1]["photo_url"],
              event_type: resp2[i+ 1]["event_type"],
              title: resp2[i + 1]["event_name"],
              zipcode: resp2[i + 1]["zipcode"],
              time: resp2[i + 1]["time"],
              com_group: resp2[i + 1]["com_group"],
              id: resp2[i + 1]["id"],
              link: "Event/"}));
              st.exampItems(exampleItems)
              st.et_handle(resp[id]["related_et"])
            }
        }
        xhr.send();
    }

    handle = (resp) => {
        this.setState( {
            response: resp
        });
    }

    et_handle = (r) => {
        if (r){
          this.setState( {
              event_types: r
          });
        }
    }

    render() {
        var id = this.props.match.params.GroupId
        var resp = this.state.response
        if (resp[id] != undefined) {
          var photo_url = resp[id]["photo_url"]
          var member_count = resp[id]["Member Count"]
          var comm_name = resp[id]["community_group_name"]
          var rating = resp[id]["rating"]
          var description = {__html: resp[id]["description"]}
        }
        return(
          <div className="root" style={styles.root}>
          <div class="container" style={styles.root}>
            <Card raised className="title-card">
            <CardHeader className="cardHeader" tag="h3">{comm_name}</CardHeader>
            <CardBody>
              <Row className="top">
                <Col>
                  <CardImg className="photo" src={photo_url}></CardImg>
                </Col>
                <Col>
                  <CardTitle tag ="h2"> Group Statistics:</CardTitle>
                  <CardTitle tag="h5"> <li>Number of Members: {member_count}</li> </CardTitle>
                  <CardTitle tag="h5"><li>Rating: <StarRatings rating={rating} starDimension="20px" starSpacing="3px"/></li>
                  </CardTitle>
                </Col>
              </Row>


              <CardTitle tag="h5">Description:</CardTitle>
              <div dangerouslySetInnerHTML={description}/>
            </CardBody>
            <CardFooter>
            <Row>
            <h3>Related Events:</h3>
              <div className="container">
              <CardDeck>
              {this.state.pageOfItems.map(item =>
               <div class="col-4">
               <Card raised className="main-card" style={{ height: '32rem' }}>
                  <CardImg src={item.img.replace('global','highres')}></CardImg>
                  <CardBody>
                      <CardTitle ><b>Title: {item.title}</b></CardTitle>
                      <CardText className="date"><b>Date:</b> {item.time} <br/>
                      <b>Zipcode:</b> {item.zipcode} <br/> <b>Community Group:</b> {item.com_group}
                      <br/> <b>Type of Event:</b> {item.event_type}</CardText>
                      <a base href={"http://" + window.location.host + "/Event/" + item.id}>Details</a>
                  </CardBody>
               </Card><br/>
               </div>
               )}
              </CardDeck>
              <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage} />
              </div>
              </Row>
              <Row>
              <h3>Related Event Types:</h3>
                <div className="container">
                  {this.state.event_types.map(item =>
                  <li><a base href={"http://" + window.location.host + "/Event1Types/" + item.id}><b>{item.name}</b></a></li>)}
                </div>
              </Row>
              </CardFooter>
            </Card>
          </div>
          </div>
        );
    }
}

export default CommunityGroupsInstance;
