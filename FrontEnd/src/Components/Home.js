import CarouCard from "./CarouCard.js";
import Splash from"./Splash.js";
import { withStyles } from "@material-ui/core/styles";
import './Home.css';
import Typography from "@material-ui/core/Typography";
import { Redirect } from 'react-router-dom';
import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import { Switch } from "@material-ui/core";
//import scrollToComponent from 'react-scroll-to-component';
import icon1 from "./img/icon1.png";
import icon2 from "./img/icon2.png";
import icon3 from "./img/icon3.png"


const styles = {
  root: {
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "rows",
    flexFlow: "row wrap",
    justifyContent: "center",
    padding: "3vh",
  }
};


class Home extends Component {


  constructor(props) {
    super(props);
    //this.scrollTo = React.createRef();

  }

  componentDidMount() {
    //const scrollToComponent = require('react-scroll-to-component');
  }



  render() {
    return (
      <div className="root" style={styles.root}>
        <div className="homepage" style={styles.root}>
        <div className="section" style={styles.root}>
        </div>
          <Splash/>
          <section id="second" className="bg-white text-center">
            <div className="container">
              <div className="row">

                <div className="col-lg-4">
                      <a href="/AllEvents">
                      <img class="img-fluid rounded-circle" src={icon1} alt="" />
                      </a>
                    <h3 style={{textTransform: 'none', fontSize: '160%'}}>All Events</h3>
                    <p className="lead mb-0" style={{fontSize: '120%'}}>Find all open events in various communities</p>

                </div>

                <div className="col-lg-4">
                      <a href="/TypesOfEvents">
                      <img class="img-fluid rounded-circle " src={icon2} alt="" />
                      </a>
                    <h3 style={{textTransform: 'none', fontSize: '160%'}}>Types of Events</h3>
                    <p className="lead mb-0" style={{fontSize: '120%'}}>Search volunteering opportunities based on Type</p>

                </div>

                <div className="col-lg-4">
                      <a href="/CommunityGroups">
                      <img class="img-fluid rounded-circle" src={icon3} alt="" />
                      </a>
                    <h3 style={{textTransform: 'none', fontSize: '160%'}}>Community Groups</h3>
                    <p className="lead mb-0" style={{fontSize: '120%'}}>Discover groups that host volunteering events</p>

                </div>

              </div>
            </div>
          </section>





        </div>
        {/* </Typography> */}
        </div>
    );
  }

}

export default Home;
