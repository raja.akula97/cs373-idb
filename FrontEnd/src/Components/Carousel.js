import React, { Component } from 'react';
import { Carousel } from "react-bootstrap";
import pic1 from './img/pic1.jpg';
import pic2 from './img/pic2.jpg';
import pic3 from './img/pic3.jpg';


class CarouselCard extends Component{
  constructor(props, context) {
    super(props, context);

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      index: 0,
      direction: null
    };
  }

  handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }

	render() {
    const { index, direction } = this.state;
		return(
  			<Carousel
          activeIndex={index}
          direction={direction}
          onSelect={this.handleSelect}
        >
          <Carousel.Item>
            <img className="img-fluid" src={pic1} />
          </Carousel.Item>
          <Carousel.Item>
            <img className="img-fluid" src={pic2} />
          </Carousel.Item>
          <Carousel.Item>
              <img className="img-fluid" src={pic3} />

          </Carousel.Item>

        </Carousel>
      );

	}
}

export default CarouselCard;
