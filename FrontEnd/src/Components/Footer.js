import React from 'react';
import './Footer.css';


import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    backgroundColor: "white"
  }
};


const Footer = () => {
  return(
    <div className='footer-container' style={styles.root}>
      <Typography variant='subtitle2' style={{color:'grey',paddingBottom:'0px'}}>Copyright © 2019 Volunteer4Me</Typography>
    </div>
  )
}

export default Footer;
