import React, { Component } from 'react';
import {Link} from 'react-router';
import Pagination from './Pagination';


import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col, CardDeck, CardHeader, CardFooter} from 'reactstrap';
import ApiCalendar from 'react-google-calendar-api';
import './Event.css';



const styles = {
  root: {
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff",
    display: "flex",
    flexDirection: "rows",
    flexFlow: "row wrap",
    justifyContent: "center",
    padding: "3vh",
  }
};


class Event extends Component{

    constructor(props) {
        super(props);

        this.state = {
          pageOfItems: [],
          response: {},
          exampleItems: {},
          sign: ApiCalendar.sign
        };

        this.onChangePage = this.onChangePage.bind(this);
        this.addEvent = this.addEvent.bind(this);
        this.signUpdate = this.signUpdate.bind(this);
            ApiCalendar.onLoad(() => {
                ApiCalendar.listenSign(this.signUpdate);
            });
    }

    signUpdate(sign: boolean): any {
            this.setState({
                sign
            })
        }
formatTime(time){
  var dict = {}
    dict["January"] = "01"
    dict["February"] = "02"
    dict["March"] = "03"
    dict["April"] = "04"
    dict["May"] = "05"
    dict["June"] = "06"
    dict["July"] = "07"
    dict["August"] = "08"
    dict["September"] = "09"
    dict["October"] = "10"
    dict["November"] = "11"
    dict["December"] = "12"
    var dateandtime = time.split(" ");
    var year = dateandtime[3];
    var month = dict[dateandtime[1]];
    var day = dateandtime[2].substring(0, dateandtime[2].length-1)
    var timeFormatted = year+"-"+month+"-"+day;
    return timeFormatted;
}

addEvent(event, time, addr, event_name){
    if(this.state.sign === false){
      ApiCalendar.handleAuthClick();
    }
    var timeFormatted = this.formatTime(time);
    var eventToAdd = {
      'summary': event_name,
      'location': addr,
      'start':{
        'date': timeFormatted
      },
      'end': {
        'date': timeFormatted
      },
      'calendarId': 'primary'
    };
    ApiCalendar.createEvent(eventToAdd)
    .then((result: eventToAdd) => {
      console.log(result);
        })
     .catch((error: any) => {
       console.log(error);
        });
  }


    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handle = (resp) => {
        this.setState( {
            response: resp
        });
    }

    exampItems = (resp) => {
        this.setState( {
            exampleItems: resp
        });
    }

    componentDidMount() {
        var id = this.props.match.params.EventNum
        let xhr = new XMLHttpRequest;
        var url = 'https://api.volunteer4.me/event?event_id=' + id
        xhr.open('GET', url, true)
        var st = this
        xhr.onload = function() {
            //check if the status is 200(means everything is okay)
            if (this.status === 200) {
              //return server response as an object with JSON.parse
              var resp = JSON.parse(this.responseText)
              st.handle(resp)
              var resp2 = resp[id]["related_events"]
              var length = Object.keys(resp2).length;
              var exampleItems = [...Array(length).keys()].map(i => ({
              img: resp2[i + 1]["photo_url"],
              event_type: resp2[i+ 1]["event_type"],
              title: resp2[i + 1]["event_name"],
              zipcode: resp2[i + 1]["zipcode"],
              time: resp2[i + 1]["time"],
              com_group: resp2[i + 1]["com_group"],
              id: resp2[i + 1]["id"],
              link: "Event/"}));
              st.exampItems(exampleItems)

            }
        }
        xhr.send();
    }
  getMapsAPILink(addr){
    var res = "https://www.google.com/maps/embed/v1/place?q="
    res += encodeURIComponent(addr)
    res+="&key="
    res+="AIzaSyAOfFoYzZ6KqxdTicU1yr4teY7IUAdIHL0"
    return res;
  }
	render() {
        var id = this.props.match.params.EventNum
        var resp = this.state.response
        if (resp[id] != undefined) {
          var photo_url = resp[id]["photo_url"].replace('global','highres')
          var event_name = resp[id]["event_name"]
          var event_type = resp[id]["event_type"]
          var com_group = resp[id]["com_group"]
          var description = {__html: resp[id]["description"]}
          var addr = resp[id]["addr"] + " " + resp[id]["city"] + " " + resp[id]["country"]
          var time = resp[id]["time"]
          var event_type_id = resp[id]["event_type_id"]
          var com_group_id = resp[id]["com_group_id"]
        }

		return(
      <div className="root" style={styles.root}>
      <div class="container" style={styles.root}>
        <Card raised className="title-card">
        <CardHeader className="cardHeader" tag="h3">{event_name}</CardHeader>
        <CardBody>
          <Row className="top">
            <Col>
              <CardImg className="photo" src={photo_url}></CardImg>
            </Col>
            <Col>
              <iframe className="map" src={this.getMapsAPILink(addr)} allowfullscreen></iframe>
            </Col>
          </Row>
          <CardTitle tag="h5">Date: {time}<br/>
          <Button onClick={(e) => {this.addEvent(e, time, addr, event_name)}}> Add to Google Calendar </Button>
          </CardTitle>
          <CardTitle tag="h5">Description:</CardTitle>
          <CardText><div dangerouslySetInnerHTML={description}/></CardText>
          <Row>
            <Col>
              <CardText raised className="event-type-card">
                <h5>Event Type: </h5><a base href={"http://" + window.location.host + "/Event1Types/" + event_type_id}>{event_type}</a>
              </CardText>
              <CardText raised className="com-group-card">
                <h5>Community Group: </h5><a base href={"http://" + window.location.host + "/CommunityGroupsInstance/" + com_group_id}>{com_group}</a>
              </CardText>
            </Col>
          </Row>
        </CardBody>
        <CardFooter>
        <Row>
        <h3>Related Events:</h3>
              <div className="container">
                <CardDeck>
                  {this.state.pageOfItems.map(item =>
                  <div class="col-4">
                  <Card raised className="main-card" style={{ height: '30rem' }}>
                    <CardImg height="200px" src={item.img.replace('global','highres')}></CardImg>
                    <CardBody>
                      <CardTitle ><b>Title: {item.title}</b></CardTitle>
                      <CardText className="date"><b>Date:</b> {item.time}
                      <br/>
                      <b>Zipcode:</b> {item.zipcode} <br/> <b>Community Group:</b> {item.com_group}
                      <br/> <b>Type of Event:</b> {item.event_type}</CardText>
                      <a base href={"http://" + window.location.host + "/Event/" + item.id}>Details</a>
                    </CardBody>
                  </Card><br/>
                  </div>)}
                </CardDeck>
                <Pagination items={this.state.exampleItems} onChangePage={this.onChangePage} />
              </div>
          </Row>
        </CardFooter>
      </Card>
      </div>
      </div>
      );


	}
}

export default Event;
