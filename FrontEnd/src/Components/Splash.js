import React from "react";
import mountain from "./img/splash_img.jpg";
import down from "./img/down_arrow.png";
import { withStyles } from "@material-ui/core/styles";
import posed from 'react-pose';
import './Splash.css';

const styles = {
    container: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      paddingBottom: "5vh"
    },
    image: {
      width: "100%",
    },
    text: {
      fontSize: "5vw",
      color: "white",
      fontFamily: 'Questrial',
    },
    button: {
      backgroundColor: "Transparent",
      border: "None",
      outlineWidth: "0 !important",
      marginTop: "10vh",
    },
    content: {
        position: "absolute",
        display: "flex",
        flexDirection: "column"
    }
};

const Text = posed.div({
   visible: { 
     opacity: 1,
     transition: { 
       ease: "easeIn",
       duration: 1500,
     }
   },
   hidden: { opacity: 0 },  
});

const Button = posed.button({
   visible: { 
     opacity: 1,
     transition: { 
       ease: "easeIn",
       duration: 1500,
     },
   },
   hidden: { opacity: 0 },  
});

class Splash extends React.Component {
  state = { 
    isHeaderVisible: false,
    isButtonVisible: false
  };

  componentDidMount() {
    this.setState({ 
      isHeaderVisible: true,
      isButtonVisible: true
    });
  }

  render() {
    const { classes, handlerFunction } = this.props;
    const { isHeaderVisible, isButtonVisible } = this.state;
    return (
      <div className={classes.container}>
        <img className={classes.image} src={mountain} alt=""/>
        <div className={classes.content}>
            <Text 
                className={classes.text}
                pose={this.state.isHeaderVisible ? 'visible' : 'hidden'}
            >Giving back to your community
            </Text>
            <a className="button_work" href="#second">
             <Button 
               className={classes.button}
               pose={this.state.isButtonVisible ? 'visible' : 'hidden'}
               id="downButton"
               onClick={handlerFunction}><img src={down} alt="" />
             </Button>
            </a>
         </div>
      </div>
    );
  }
}

export default withStyles(styles)(Splash);
