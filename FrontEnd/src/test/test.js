import React from 'react';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import Footer from '../Components/Footer.js'
import AllEvents from '../Components/AllEvents.js'
import TypesOfEvents from '../Components/TypesOfEvents.js'
import CommunityGroups from '../Components/CommunityGroups.js'
import Event1Types from '../Components/Event1Types.js'
import GitTable from '../Components/GitTable.js'
import Event from '../Components/Event.js'
import CommunityGroupsInstance from '../Components/CommunityGroupsInstance.js'


describe('Footer Component', () => {
  //Test 1
  it('Typography', () => {
  	const wrapper = shallow(<Footer />)
    expect(!wrapper.find('Typography'))
  })

  // Test 2
  it('title check', () => {
  	const wrapper = shallow(<Footer />)
    expect(wrapper.prop('title')).equal(undefined)
  })
})

describe('AllEvents Component', () => {
  // Test 3
  it('CardTitle check', () => {
  	const wrapper = shallow(<AllEvents />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 4
  it('CardText check', () => {
  	const wrapper = shallow(<AllEvents />)
    expect(wrapper.prop('text')).equal(undefined)
  })

  // Test 5
  it('CardBody check', () => {
    const wrapper = shallow(<AllEvents />)
    expect(wrapper.prop('title')).equal(undefined)
  })
})

describe('TypesOfEvents Component', () => {
  // Test 6
  it('CardTitle check', () => {
    const wrapper = shallow(<TypesOfEvents />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 7
  it('CardBody check', () => {
  	const wrapper = shallow(<TypesOfEvents />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 8
  it('CardText check', () => {
  	const wrapper = shallow(<TypesOfEvents />)
    expect(wrapper.prop('text')).equal(undefined)
  })
})

describe('CommunityGroups Component', () => {

  // Test 9
  it('CardBody check', () => {
    const wrapper = shallow(<CommunityGroups />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 10
  it('CardTitle check', () => {
  	const wrapper = shallow(<CommunityGroups />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 11
  it('Card check', () => {
  	const wrapper = shallow(<CommunityGroups/>)
    expect(wrapper.prop('text')).equal(undefined)
  })
})

describe('GitTable Component', () => {
  // Test 12
  it('Table check', () => {
  	const wrapper = shallow(<GitTable />)
    expect(wrapper.prop('title')).equal(undefined)
  })

  // Test 13
  it('td check', () => {
  	const wrapper = shallow(<GitTable/>)
    expect(wrapper.prop('text')).equal(undefined)
  })
})


describe('Event Instance', () => {
  // Test 14
  it('signUpdate method check', () => {
    let event = new Event();
    expect(!event.signUpdate())
  })

  // Test 15
  it('map api check for adding event', () => {
    let event = new Event();
    expect(event.getMapsAPILink("hello")).equal("https://www.google.com/maps/embed/v1/place?q=hello&key=AIzaSyAOfFoYzZ6KqxdTicU1yr4teY7IUAdIHL0")
  })
})

describe('Types of event Instance', () => {
  // Test 16
  it('Check if function exists in event type', () => {
    let type = new Event1Types();
    expect(type.exampItems())
  })

  // Test 17
  it('Handle function check', () => {
    let type = new Event1Types();
    expect(type.handle())
  })

  // Test 18
  it('group handle function check', () => {
    let type = new Event1Types();
    expect(type.group_handle())
  })
})

describe('Community Group Instance', () => {
  // Test 19
  it('Handle function check', () => {
    let group = new CommunityGroupsInstance();
    expect(group.handle())
  })

  // Test 20
  it('event type handle check', () => {
    let group = new CommunityGroupsInstance();
    expect(group.et_handle())
  })
})





