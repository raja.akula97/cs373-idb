from flask import Flask, jsonify, request, Response
import requests, json, collections, datetime, psycopg2, sys, time
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String, Float, desc, asc, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from db_objects import AllEvents, EventTypes, CommunityGroups


engine = create_engine(
    "postgresql+psycopg2://root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbdb"
)
Session = sessionmaker(bind=engine)


def all_event_types_handler(
    sort_type, sort_order, rating, num_events, num_groups, search_term
):
    session = Session()
    query = session.query(EventTypes)

    if search_term:
        query = query.filter(or_(EventTypes.name.ilike(search_term)))

    if num_events:
        if num_events == "large":
            query = query.filter(EventTypes.num_events > 50)
        elif num_events == "medium":
            query = query.filter(EventTypes.num_events > 20).filter(
                EventTypes.num_events <= 50
            )
        else:
            query = query.filter(EventTypes.num_events <= 20)

    if num_groups:
        if num_groups == "large":
            query = query.filter(EventTypes.num_groups > 50)
        elif num_groups == "medium":
            query = query.filter(EventTypes.num_groups > 20).filter(
                EventTypes.num_groups <= 50
            )
        else:
            query = query.filter(EventTypes.num_groups <= 20)

    if rating:
        query = query.filter(EventTypes.avg_rating >= float(rating))

    if sort_type and sort_order == "True":
        query = query.order_by(desc(sort_type))

    if sort_type:
        query = query.order_by(sort_type)

    response_list = list()
    for row in query.all():
        response_list.append(row.to_dict())
    session.close()
    return Response(json.dumps(response_list), status=200, mimetype="application/json")


def type_id_handler(type_id):
    try:
        type_id_int = int(type_id)
        session = Session()
        query = session.query(EventTypes).filter(EventTypes.id == type_id_int)
        if len(query.all()) == 0:
            response_str = (
                '{"Error": "The given type_id couldn\'t be found in the database"}'
            )
            return Response(response_str, status=404, mimetype="application/json")
        response_dict = dict()
        for row in query.all():
            inner_dict = row.to_full_dict()
            response_dict[row.id] = inner_dict

            session2 = Session()
            related_events_dictionary = dict()
            inner_dict["related_events"] = related_events_dictionary
            related_com_groups = []
            inner_dict["related_com_groups"] = related_com_groups
            query2 = session.query(AllEvents).filter_by(event_type=row.name).limit(3)
            count = 1
            for row2 in query2.all():
                if row2.event_type == row.name:
                    related_event_info = row2.to_dict()
                    related_events_dictionary[count] = related_event_info

                    q = (
                        session.query(CommunityGroups)
                        .filter_by(name=row2.com_group)
                        .limit(1)
                    )
                    for r in q.all():
                        related_com_groups.append({"id": r.id, "name": r.name})
                    count += 1
            session2.close()
        session.close()
        return Response(
            json.dumps(response_dict), status=200, mimetype="application/json"
        )
    except ValueError:
        return Response(
            "{'Error': 'type_id passed in can't be converted to int'}",
            status=400,
            mimetype="application/json",
        )
