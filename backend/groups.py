from flask import Flask, jsonify, request, Response
import requests, json, collections, datetime, psycopg2, sys, time
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String, Float, desc, asc, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from db_objects import AllEvents, EventTypes, CommunityGroups

engine = create_engine(
    "postgresql+psycopg2://root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbdb"
)
Session = sessionmaker(bind=engine)


def all_groups_handler(member_count, rating, sort_type, sort_order, search_term):
    session = Session()
    query = session.query(CommunityGroups)

    if search_term:
        query = query.filter(or_(CommunityGroups.name.ilike(search_term)))

    if member_count:
        if member_count == "large":
            query = query.filter(CommunityGroups.member_count > 10000)
        elif member_count == "medium":
            query = query.filter(CommunityGroups.member_count > 200).filter(
                CommunityGroups.member_count < 10000
            )
        else:
            query = query.filter(CommunityGroups.member_count < 200)

    if rating:
        query = query.filter(CommunityGroups.rating >= float(rating))

    if sort_type and sort_order == "True":
        query = query.order_by(desc(sort_type))
    if sort_type:
        query = query.order_by(sort_type)

    response_list = list()
    for row in query.all():
        response_list.append(row.to_dict())
    session.close()
    return Response(json.dumps(response_list), status=200, mimetype="application/json")


def group_id_handler(group_id):
    try:
        group_id_int = int(group_id)
        session = Session()
        query = session.query(CommunityGroups).filter(
            CommunityGroups.id == group_id_int
        )
        if len(query.all()) == 0:
            response_str = (
                '{"Error": "The given group_id couldn\'t be found in the database"}'
            )
            return Response(response_str, status=404, mimetype="application/json")
        response_dict = dict()
        for row in query.all():
            inner_dict = row.to_full_dict()
            response_dict[row.id] = inner_dict
            session2 = Session()
            related_events_dictionary = dict()
            inner_dict["related_events"] = related_events_dictionary
            related_et = []
            inner_dict["related_et"] = related_et
            query2 = (
                session.query(AllEvents)
                .filter(AllEvents.com_group == row.name)
                .limit(3)
            )
            count = 1
            for row2 in query2.all():
                if row2.com_group == row.name:
                    related_event_info = row2.to_dict()
                    related_events_dictionary[count] = related_event_info
                    q = (
                        session.query(EventTypes)
                        .filter_by(name=row2.event_type)
                        .limit(1)
                    )
                    for r in q.all():
                        related_et.append({"id": r.id, "name": r.name})
                    count += 1
            session2.close()
        session.close()
        return Response(
            json.dumps(response_dict), status=200, mimetype="application/json"
        )
    except ValueError:
        return Response(
            "{'Error': 'group_id passed in can't be converted to int'}",
            status=400,
            mimetype="application/json",
        )
