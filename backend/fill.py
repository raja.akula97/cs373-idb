import requests, json, psycopg2
from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from db_objects import EventTypes, AllEvents, CommunityGroups, Base
import time as t
import os
import googleapiclient

# Create engine connection
engine = create_engine(
    "postgresql+psycopg2://root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbdb"
)
Session = sessionmaker(bind=engine)


def add_event(
    e_name, e_type, group, descrip, address, ct, st, cy, zipc, dt, photo, m_id
):
    session = Session()
    exists = session.query(AllEvents.id).filter_by(meetup_id=m_id).scalar() is not None
    if not exists:
        event = AllEvents(
            event_name=e_name,
            event_type=e_type,
            com_group=group,
            description=descrip,
            addr=address,
            city=ct,
            state=st,
            country=cy,
            zipcode=zipc,
            time=dt,
            photo_url=photo,
            meetup_id=m_id,
        )
        session.add(event)
        session.commit()
    session.close()


def add_group(g_name, descrip, m_id, photo, m_count, rating, e_count=-1):
    session = Session()
    exists = (
        session.query(CommunityGroups.id).filter_by(meetup_id=m_id).scalar() is not None
    )
    if not exists:
        group = CommunityGroups(
            name=g_name,
            meetup_id=m_id,
            photo_url=photo,
            rating=rating,
            description=descrip,
            member_count=m_count,
            num_events=e_count,
        )
        session.add(group)
        session.commit()
    session.close()


def add_type(event_name, descrip, m_id, photo, rating=-1, enum=-1, gnum=-1, v_id=-1):
    session = Session()
    exists = session.query(EventTypes.id).filter_by(meetup_id=m_id).scalar() is not None
    if not exists:
        type_event = EventTypes(
            name=event_name,
            description=descrip,
            meetup_id=m_id,
            photo_url=photo,
            avg_rating=rating,
            num_events=enum,
            num_groups=gnum,
            video_id=v_id,
        )
        # print('adding')
        session.add(type_event)
        # print("are we getting here")
        session.commit()
    session.close()


def add_event_count_type():
    session = Session()

    sql_command = "UPDATE typesv1 set num_events=%s WHERE name=%s"
    connection = engine.connect()

    types = session.query(EventTypes.name)
    for t in types:
        events = session.query(AllEvents.id).filter_by(event_type=t).all()
        values = (len(events), t)
        connection.execute(sql_command, values)

    session.commit()
    connection.close()
    session.close()


def add_group_count_type():
    session = Session()

    sql_command = "UPDATE typesv1 set num_groups=%s, avg_rating=%s WHERE name=%s"
    connection = engine.connect()

    types = session.query(EventTypes.name)
    for t in types:
        groups = session.query(AllEvents.com_group).filter_by(event_type=t).all()
        rating = []
        for g in groups:
            rate = session.query(CommunityGroups.rating).filter_by(name=g).all()[0][0]
            rate = float(rate)
            if rate > 0:
                rating.append(float(rate))

        avg = sum(rating) / len(rating)
        count = len(set(groups))
        values = (count, avg, t)
        connection.execute(sql_command, values)

    session.commit()
    connection.close()
    session.close()


def add_event_count_groups():
    session = Session()

    sql_command = "UPDATE groupsv1 set num_events=%s WHERE name=%s"
    connection = engine.connect()

    groups = session.query(CommunityGroups.name)
    for g in groups:
        events = session.query(AllEvents.id).filter_by(com_group=g).all()
        values = (len(events), g)
        connection.execute(sql_command, values)

    session.commit()
    connection.close()
    session.close()


# This method is to ping meetup URL to get response headers. This check is necessary because meetup doesn't allow
# too many requests at once. Returns wait time, which is later used in "sleep"
def check_wait():
    url = "https://api.meetup.com/2/topic_categories?key=c637716767c20772138184c116fa&sign=true&photo-host=public"
    response = requests.get(url)
    if int(response.headers["X-RateLimit-Remaining"]) < 5:
        return response.headers["X-RateLimit-Reset"]
    return "0"


def ping_topic_categories():
    topic_photos = []  # Keep track of the topics to call all events later

    # Ping meetup API for Topic categories, we need this to get photo_urls.
    url = "https://api.meetup.com/2/topic_categories?key=c637716767c20772138184c116fa&sign=true&photo-host=public"
    response = requests.get(url).json()
    for topic in response["results"]:
        photo_url = topic["icon"]["highres_link"]
        name = topic["name"]
        categories = topic["category_ids"]
        tup = (name, photo_url, categories)
        topic_photos.append(tup)

    # Get categories,
    categories = []
    response = requests.get(
        "https://api.meetup.com/2/categories?key=c637716767c20772138184c116fa&sign=true&photo-host=public"
    )
    content = response.json()["results"]
    for cat in content:
        mid = cat["id"]
        name = cat["name"]
        print(str(mid) + " " + name)
        query = "Define " + str(name)
        serp_url = (
            "https://serpapi.com/search.json?q=%s&api_key=152e9986ee0e3eee86b75bfbfa5129a0ec383979e2ce1a25aa845f1136927e14"
            % (query,)
        )
        serp_resp = requests.get(serp_url).json()
        description = "NA"
        if "answer_box" in serp_resp:
            if serp_resp["answer_box"]["type"] == "organic_result":
                description = serp_resp["answer_box"]["snippet"]
            elif serp_resp["answer_box"]["type"] == "dictionary_results":
                description = ""
                for definition in serp_resp["answer_box"]["definitions"]:
                    new_def = definition + "<br>"
                    description += new_def

        if description == "NA":
            continue

        # Get photo
        photo_url = "NA"
        for topic in topic_photos:
            if mid in topic[2]:
                photo_url = topic[1]
                break

        if photo_url == "NA":
            continue

        # print("about to add")
        add_type(event_name=name, descrip=description, m_id=mid, photo=photo_url)
        tup = (mid, name)
        categories.append(tup)

    return categories


def ping_all_events(categories):
    comgroups = []
    for cat in categories:
        seconds = check_wait()
        t.sleep(int(seconds))

        url = (
            "https://api.meetup.com/2/open_events?key=e732d3a641d576082d326a62d174&sign=true&photo-host=public&state=TX&text=volunteer&category=%s&radius=100"
            % (cat[0],)
        )

        response = requests.get(url)
        content = response.json()
        if content["meta"]["total_count"] == 0:
            continue

        data = content["results"]
        for event in data:
            if (
                "name" not in event
                or "venue" not in event
                or "time" not in event
                or "photo_url" not in event
                or "description" not in event
            ):
                continue
            if (
                "address_1" not in event["venue"]
                or "city" not in event["venue"]
                or "state" not in event["venue"]
                or "localized_country_name" not in event["venue"]
            ):
                continue
            if "zip" not in event["venue"]:
                continue

            addr = event["venue"]["address_1"]
            city = event["venue"]["city"]
            state = event["venue"]["state"]
            country = event["venue"]["localized_country_name"]
            zipcode = event["venue"]["zip"]
            time = event["time"]
            photo_url = event["photo_url"]
            meetup_id = event["id"]
            name = event["name"]
            event_type = cat[1]
            com_group = event["group"]["name"]
            group_id = event["group"]["id"]
            tup = (group_id, com_group)
            comgroups.append(tup)
            description = event["description"]
            add_event(
                e_name=name,
                e_type=event_type,
                group=com_group,
                descrip=description,
                address=addr,
                ct=city,
                st=state,
                cy=country,
                zipc=zipcode,
                dt=time,
                photo=photo_url,
                m_id=meetup_id,
            )

    return comgroups


def ping_groups(comgroups):
    for g in comgroups:
        seconds = check_wait()
        t.sleep(int(seconds))

        url = (
            "https://api.meetup.com/2/groups?key=c637716767c20772138184c116fa&sign=true&photo-host=public&group_id=%s"
            % (g[0],)
        )
        response = requests.get(url)
        data = response.json()
        if "results" not in data:
            continue
        for group in data["results"]:
            name = g[1]
            meetup_id = "-1"
            photo_url = "https://d.wildapricot.net/images/newsblog/bigstock-portrait-of-a-happy-and-divers-19389686.jpg?v=0"
            rating = "-1"
            member_count = "-1"
            description = "NA"
            if "id" in group:
                meetup_id = group["id"]
            if "group_photo" in group:
                photo_url = group["group_photo"]["photo_link"]
            if "rating" in group:
                rating_ = group["rating"]
            if "description" in group:
                description = group["description"]
            if "members" in group:
                member_count = group["members"]

            add_group(
                g_name=name,
                m_id=meetup_id,
                photo=photo_url,
                rating=rating_,
                descrip=description,
                m_count=member_count,
            )
    return


def add_video_types(categories):
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = "AIzaSyBcVA1S6lmpQEzhyIVjRZsk5OOaEAiyHfk"
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY
    )

    session = Session()
    sql_command = "UPDATE typesv1 set video_id=%s WHERE name=%s"
    connection = engine.connect()

    for c in categories:
        search_query = c[1]
        request = youtube.search().list(
            part="snippet", maxResults=25, q=search_query, type="video"
        )
        response = request.execute()
        video_id = response["items"][0]["id"]["videoId"]
        values = (video_id, search_query)
        connection.execute(sql_command, values)

    session.commit()
    connection.close()
    session.close()


def main():
    Base.metadata.create_all(engine)

    # Get the topic categories and add fill base EventTypes table
    print("Calling categories and filling types table")
    categories = ping_topic_categories()
    print("Done")

    # Using the categories, ping the all events and fill AllEvents table.
    print("Getting events and filling Allevents table")
    comgroups = ping_all_events(categories)
    print("Done")

    # Using the list of groups, ping the groups api and fill Community Groups
    print("Calling groups and filling groups table")
    ping_groups(comgroups)
    print("Calling categories")

    # Now we have all the base info from meetup filled
    # Make calculations and fill the numbers for types and groups

    # Adding num_events to types table
    print("Num events types")
    add_event_count_type()

    # Adding num_groups to types table
    print("Num groups types")
    add_group_count_type()

    # Adding num_events to groups table
    print("Num events groups")
    add_event_count_groups()

    # Adding youtube video id to types
    print("Video youtube types")
    add_video_types(categories)


if __name__ == "__main__":
    main()
