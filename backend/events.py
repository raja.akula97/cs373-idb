from flask import Flask, jsonify, request, Response
import requests, json, collections, datetime, psycopg2, sys, time
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String, Float, desc, asc, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from db_objects import AllEvents, EventTypes, CommunityGroups

engine = create_engine(
    "postgresql+psycopg2://root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbdb"
)
Session = sessionmaker(bind=engine)


def all_events_handler(
    zipcode, date_begin, date_end, event_type, sort_type, sort_order, search_term
):
    session = Session()
    query = session.query(AllEvents)

    # Filtering by different params
    if search_term:
        query = query.filter(
            or_(
                AllEvents.event_name.ilike(search_term),
                AllEvents.city.ilike(search_term),
                AllEvents.zipcode.ilike(search_term),
            )
        )
    if zipcode:
        query = query.filter(AllEvents.zipcode == zipcode)
    if date_begin and date_end:
        d1 = datetime.strptime(date_begin, "%Y-%m-%d").timestamp() * 1000
        d2 = (datetime.strptime(date_end, "%Y-%m-%d") + timedelta(1)).timestamp() * 1000
        query = query.filter(AllEvents.time <= str(d2)).filter(
            AllEvents.time >= str(d1)
        )
    if event_type:
        query = query.filter(AllEvents.event_type == event_type)

    # Sorting by Date
    if sort_type and sort_order == "True":
        query = query.order_by(desc(sort_type))
    if sort_type:
        query = query.order_by(asc(sort_type))

    # if sort_az == 'True':
    #     query = query.order_by(AllEvents.event_name)
    # elif sort_az == 'False':
    #     query = query.order_by(AllEvents.event_name.desc())
    # elif :
    #     query = query.order_by(AllEvents.event_name)
    #     query_list = query.all().reverse()
    response_list = list()
    for row in query.all():
        response_list.append(row.to_dict())
    session.close()
    return Response(json.dumps(response_list), status=200, mimetype="application/json")


def event_id_handler(event_id):
    try:
        event_id_int = int(event_id)
        session = Session()
        query = session.query(AllEvents).filter(AllEvents.id == event_id_int)
        response_dict = dict()
        if len(query.all()) == 0:
            response_str = (
                '{"Error": "The given event_id couldn\'t be found in the database"}'
            )
            return Response(response_str, status=404, mimetype="application/json")
        for row in query.all():
            inner_dict = row.to_full_dict()
            response_dict[row.id] = inner_dict

            session_id = Session()
            event_type_id = (
                session_id.query(EventTypes)
                .filter(row.event_type == EventTypes.name)
                .limit(1)
                .all()[0]
                .id
            )
            com_group_id = (
                session_id.query(CommunityGroups)
                .filter(row.com_group == CommunityGroups.name)
                .limit(1)
                .all()[0]
                .id
            )

            inner_dict["event_type_id"] = event_type_id
            inner_dict["com_group_id"] = com_group_id
            session_id.close()

            related_events_dict = dict()
            inner_dict["related_events"] = related_events_dict
            session2 = Session()
            query2 = session2.query(AllEvents).filter(
                AllEvents.event_type == row.event_type
            )
            shortenedQuery = query2.all()[:3]
            count = 1
            for row2 in shortenedQuery:
                if row2.id != row.id:
                    related_inner_dict = row2.to_dict()
                    related_events_dict[count] = related_inner_dict
                    count += 1
            session2.close()
        session.close()
        return Response(
            json.dumps(response_dict), status=200, mimetype="application/json"
        )
    except ValueError:
        return Response(
            "{'Error': 'event_id passed in can't be converted to int'}",
            status=400,
            mimetype="application/json",
        )
