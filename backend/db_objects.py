from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
import datetime


Base = declarative_base()


def date_helper(d):
    date_obj = datetime.datetime.fromtimestamp(int(str(d)[:-3]))
    return date_obj.strftime("%A %B %d, %Y")


class AllEvents(Base):
    __tablename__ = "eventsv1"
    id = Column(Integer, primary_key=True)
    event_name = Column(String)
    event_type = Column(String)
    com_group = Column(String)
    description = Column(String)
    addr = Column(String)
    city = Column(String)
    state = Column(String)
    country = Column(String)
    zipcode = Column(String)
    time = Column(String)
    photo_url = Column(String)
    meetup_id = Column(String)

    def __repr__(self):
        return (
            "<AllEvents(Event Name='%s', Event Type='%s', Community Group='%s', Description='%s', Address='%s', Time='%s')>"
            % (
                self.event_name,
                self.event_type,
                self.com_group,
                self.description,
                self.addr,
                self.time,
            )
        )

    def to_dict(self):
        inner_dict = dict()
        inner_dict["id"] = self.id
        inner_dict["event_name"] = self.event_name
        inner_dict["zipcode"] = self.zipcode
        inner_dict["com_group"] = self.com_group
        inner_dict["photo_url"] = self.photo_url
        inner_dict["time"] = date_helper(self.time)
        inner_dict["event_type"] = self.event_type
        inner_dict["state"] = self.state
        return inner_dict

    def to_full_dict(self):
        inner_dict = self.to_dict()
        inner_dict["description"] = self.description
        inner_dict["addr"] = self.addr
        inner_dict["city"] = self.city
        inner_dict["state"] = self.state
        inner_dict["country"] = self.country
        return inner_dict


class EventTypes(Base):
    __tablename__ = "typesv1"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    meetup_id = Column(Integer)
    photo_url = Column(String)
    avg_rating = Column(Float)
    num_events = Column(Integer)
    num_groups = Column(Integer)
    video_id = Column(String)

    def to_dict(self):
        d = self.to_full_dict()
        d["description"] = d["description"][:30]
        return d

    def to_full_dict(self):
        inner_dict = dict()
        inner_dict["event_type_name"] = self.name
        inner_dict["description"] = self.description
        inner_dict["photo_url"] = self.photo_url
        inner_dict["id"] = self.id
        inner_dict["video_id"] = self.video_id
        inner_dict["num_events"] = self.num_events
        inner_dict["num_groups"] = self.num_groups
        inner_dict["avg_group_rating"] = self.avg_rating
        return inner_dict


class CommunityGroups(Base):
    __tablename__ = "groupsv1"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    meetup_id = Column(Integer)
    photo_url = Column(String)
    rating = Column(Float)
    description = Column(String)
    member_count = Column(Integer)
    num_events = Column(Integer)

    def to_dict(self):
        d = self.to_full_dict()
        d["description"] = d["description"][:30]
        return d

    def to_full_dict(self):
        inner_dict = dict()
        inner_dict["community_group_name"] = self.name
        inner_dict["photo_url"] = self.photo_url
        inner_dict["description"] = self.description
        inner_dict["rating"] = self.rating
        inner_dict["Member Count"] = self.member_count
        inner_dict["num_events"] = self.num_events
        inner_dict["id"] = self.id
        return inner_dict
