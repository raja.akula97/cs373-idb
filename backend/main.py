from flask import Flask, jsonify, request, Response
import requests, json, collections, datetime, psycopg2, sys, time
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String, Float, desc, asc, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from db_objects import AllEvents, EventTypes, CommunityGroups

from groups import all_groups_handler, group_id_handler
from events import all_events_handler, event_id_handler
from event_types import all_event_types_handler, type_id_handler


app = Flask(__name__)
CORS(app)
engine = create_engine(
    "postgresql+psycopg2://root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbdb"
)
Session = sessionmaker(bind=engine)


def date_helper(d):
    date_obj = datetime.fromtimestamp(int(str(d)[:-3]))
    return date_obj.strftime("%A %B %d, %Y")


@app.route("/")
def hello():
    return jsonify({"about": "Hello, World!"})


@app.route("/event", methods=["GET"])
def event_api_router():
    all_bool = request.args.get("all")
    event_id = request.args.get("event_id")
    zipcode = request.args.get("zipcode")
    date_begin = request.args.get("date_b")
    date_end = request.args.get("date_e")
    sort_az = request.args.get("alpha_sort")
    sort_type = request.args.get("sortby")
    sort_order = request.args.get("sortrev")
    event_type = request.args.get("event_type")
    search_term = request.args.get("search")
    if search_term:
        search_term = "%" + search_term + "%"
    if all_bool:
        return all_events_handler(
            zipcode,
            date_begin,
            date_end,
            event_type,
            sort_type,
            sort_order,
            search_term,
        )
    else:
        return event_id_handler(event_id)


@app.route("/event/showValidParams")
def get_event_parameters():
    sortables = ["event_name", "event_type", "com_group", "time", "zipcode"]
    response_dict = dict()
    response_dict["sortby"] = sortables
    return Response(json.dumps(response_dict), status=200, mimetype="application/json")


@app.route("/communityGroups", methods=["GET"])
def community_groups_api_router():
    all_bool = request.args.get("all")
    group_id = request.args.get("group_id")
    member_count = request.args.get("member_count")
    rating = request.args.get("rating")
    sort_type = request.args.get("sortby")
    sort_order = request.args.get("sortrev")
    search_term = request.args.get("search")
    if search_term:
        search_term = "%" + search_term + "%"
    if all_bool:
        return all_groups_handler(
            member_count, rating, sort_type, sort_order, search_term
        )
    else:
        return group_id_handler(group_id)


@app.route("/communityGroups/showValidParams")
def get_community_groups_parameters():
    sortables = ["name", "rating", "member_count", "num_events"]
    response_dict = dict()
    response_dict["sortby"] = sortables
    response_dict["member_count"] = ["small", "medium", "large"]
    return Response(json.dumps(response_dict), status=200, mimetype="application/json")


@app.route("/eventTypes")
def event_types_api_router():
    all_bool = request.args.get("all")
    type_id = request.args.get("type_id")
    rating = request.args.get("rating")
    num_events = request.args.get("num_events")
    num_groups = request.args.get("num_groups")
    sort_type = request.args.get("sortby")
    sort_order = request.args.get("sortrev")
    search_term = request.args.get("search")
    if search_term:
        search_term = "%" + search_term + "%"
    if all_bool:
        return all_event_types_handler(
            sort_type, sort_order, rating, num_events, num_groups, search_term
        )
    else:
        return type_id_handler(type_id)


@app.route("/eventTypes/showValidParams")
def get_event_types_parameters():
    sortables = ["name", "avg_rating", "num_events", "num_groups"]
    response_dict = dict()
    response_dict["sortby"] = sortables
    response_dict["num_events"] = ["small", "medium", "large"]
    response_dict["num_groups"] = response_dict["num_events"]
    return Response(json.dumps(response_dict), status=200, mimetype="application/json")


@app.route("/search")
def search():
    kwd = request.args.get("kwd")
    search_term = "%" + kwd + "%"
    session = Session()

    response_dict = dict()

    # Events Search
    query = session.query(AllEvents).filter(
        or_(
            AllEvents.event_name.ilike(search_term),
            AllEvents.zipcode.ilike(search_term),
        )
    )
    event_list = list()
    response_dict["events"] = event_list
    for row in query.all():
        event_list.append(row.to_dict())

    # Event Types Search
    query = session.query(EventTypes).filter(or_(EventTypes.name.ilike(search_term)))
    et_list = list()
    response_dict["event_types"] = et_list
    for row in query.all():
        et_list.append(row.to_dict())

    # Community Groups Search
    query = session.query(CommunityGroups).filter(
        or_(CommunityGroups.name.ilike(search_term))
    )
    cm_list = list()
    response_dict["community_groups"] = cm_list
    for row in query.all():
        cm_list.append(row.to_dict())

    session.close()
    return Response(json.dumps(response_dict), status=200, mimetype="application/json")


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=80)
