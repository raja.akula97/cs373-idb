import os
import googleapiclient.discovery


def main():
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = "AIzaSyBcVA1S6lmpQEzhyIVjRZsk5OOaEAiyHfk"

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY
    )

    request = youtube.search().list(
        part="snippet", maxResults=25, q="surfing", type="video"
    )
    response = request.execute()

    print(response["items"][0]["id"]["videoId"])


main()
