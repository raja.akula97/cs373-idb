import unittest
import flask_testing
import os
from flask import Flask, Response
from main import AllEvents, EventTypes, CommunityGroups, Session
from main import event_id_handler, type_id_handler, group_id_handler
from fill import add_event, add_group, add_type

session = Session()


class AllTests(flask_testing.TestCase):
    db = "root:volunteer4me.@volunteer4medb.c4qhmtnxkqfq.us-east-1.rds.amazonaws.com:5432/dbd"

    def create_app(self):
        app = Flask(__name__)
        return app

    def tearDown(self):
        return self

    def test_add_event_types(self):
        e_type = EventTypes(
            name="Test Type",
            description="This is a test type for unit testing",
            meetup_id=-1,
            photo_url="No URL provided",
            avg_rating=4.66,
            num_events=0,
            num_groups=0,
        )
        session.add(e_type)

        # Get e_type and assert json
        response = session.query(EventTypes).filter_by(meetup_id="-1").first()
        self.assertEqual(response.name, "Test Type")
        self.assertEqual(response.description, "This is a test type for unit testing")
        self.assertEqual(response.meetup_id, -1)
        self.assertEqual(response.photo_url, "No URL provided")
        self.assertEqual(response.avg_rating, 4.66)
        self.assertEqual(response.num_events, 0)
        self.assertEqual(response.num_groups, 0)

        session.delete(response)
        session.commit()

    def test_add_event(self):
        event = AllEvents(
            event_name="Test Event",
            event_type="Test Type",
            com_group="Test Group",
            description="This is a test event for unit testing",
            addr="Test Address",
            city="Austin",
            state="TX",
            country="USA",
            zipcode="78705",
            time="Test Date Time",
            photo_url="No URL provided",
            meetup_id="-2",
        )
        session.add(event)

        # Get event and assert json
        response = session.query(AllEvents).filter_by(meetup_id="-2").first()
        self.assertEqual(response.event_name, "Test Event")
        self.assertEqual(response.event_type, "Test Type")
        self.assertEqual(response.com_group, "Test Group")
        self.assertEqual(response.description, "This is a test event for unit testing")
        self.assertEqual(response.addr, "Test Address")
        self.assertEqual(response.city, "Austin")
        self.assertEqual(response.state, "TX")
        self.assertEqual(response.country, "USA")
        self.assertEqual(response.zipcode, "78705")
        self.assertEqual(response.time, "Test Date Time")
        self.assertEqual(response.meetup_id, "-2")
        self.assertEqual(response.photo_url, "No URL provided")

        session.delete(response)
        session.commit()

    def test_add_group(self):
        group = CommunityGroups(
            name="Test Group",
            meetup_id=-3,
            photo_url="No URL provided",
            rating=6.00,
            description="This is a test group for unit tests",
            member_count=0,
            num_events=0,
        )
        session.add(group)

        # Get group and assert json
        response = session.query(CommunityGroups).filter_by(meetup_id="-3").first()
        self.assertEqual(response.name, "Test Group")
        self.assertEqual(response.meetup_id, -3)
        self.assertEqual(response.photo_url, "No URL provided")
        self.assertEqual(response.rating, 6.00)
        self.assertEqual(response.description, "This is a test group for unit tests")
        self.assertEqual(response.member_count, 0)
        self.assertEqual(response.num_events, 0)

        session.delete(response)
        session.commit()

    # Testing using fill.py methods
    def test_add_group_fill(self):
        name = "Test Group"
        meetup_id = (-4,)
        photo_url = ("No URL provided",)
        rating = (6.00,)
        description = ("This is a test group for unit tests",)
        member_count = (0,)
        num_events = 0
        add_group(
            g_name=name,
            m_id=meetup_id,
            photo=photo_url,
            rating=rating,
            descrip=description,
            m_count=member_count,
            e_count=num_events,
        )

        # Get group and assert json
        response = session.query(CommunityGroups).filter_by(meetup_id="-4").first()
        self.assertEqual(response.name, "Test Group")
        self.assertEqual(response.meetup_id, -4)
        self.assertEqual(response.photo_url, "No URL provided")
        self.assertEqual(response.rating, 6.00)
        self.assertEqual(response.description, "This is a test group for unit tests")
        self.assertEqual(response.member_count, 0)
        self.assertEqual(response.num_events, 0)

        session.delete(response)
        session.commit()

    def test_add_event_types_fill(self):
        name = "Test Type"
        description = ("This is a test type for unit testing",)
        meetup_id = -5
        photo_url = ("No URL provided",)
        avg_rating = 4.66
        num_events = 0
        num_groups = 0
        video_id = 111
        add_type(
            event_name=name,
            m_id=meetup_id,
            photo=photo_url,
            descrip=description,
            rating=avg_rating,
            enum=num_events,
            gnum=num_groups,
            v_id=video_id,
        )

        # Get e_type and assert json
        response = session.query(EventTypes).filter_by(meetup_id="-5").first()
        self.assertEqual(response.name, "Test Type")
        self.assertEqual(response.description, "This is a test type for unit testing")
        self.assertEqual(response.meetup_id, -5)
        self.assertEqual(response.photo_url, "No URL provided")

        session.delete(response)
        session.commit()

    def test_add_event_fill(self):
        name = "Test Event"
        e_type = "Test Type"
        cgroup = "Test Group"
        description = "This is a test event for unit testing"
        address = "Test Address"
        city = "Austin"
        state = "TX"
        country = "USA"
        zipcode = "78705"
        time = "Test Date Time"
        photo_url = "No URL provided"
        meetup_id = "-6"

        add_event(
            e_name=name,
            e_type=e_type,
            group=cgroup,
            descrip=description,
            address=address,
            ct=city,
            st=state,
            cy=country,
            zipc=zipcode,
            dt=time,
            photo=photo_url,
            m_id=meetup_id,
        )

        # Get event and assert json
        # Get event and assert json
        response = session.query(AllEvents).filter_by(meetup_id="-6").first()
        self.assertEqual(response.event_name, "Test Event")
        self.assertEqual(response.event_type, "Test Type")
        self.assertEqual(response.com_group, "Test Group")
        self.assertEqual(response.description, "This is a test event for unit testing")
        self.assertEqual(response.addr, "Test Address")
        self.assertEqual(response.city, "Austin")
        self.assertEqual(response.state, "TX")
        self.assertEqual(response.country, "USA")
        self.assertEqual(response.zipcode, "78705")
        self.assertEqual(response.time, "Test Date Time")
        self.assertEqual(response.meetup_id, "-6")
        self.assertEqual(response.photo_url, "No URL provided")

        session.delete(response)
        session.commit()

    def test_add_group_fill_2(self):
        name = "Test Group"
        meetup_id = (-4,)
        photo_url = ("No URL provided",)
        rating = (6.00,)
        description = ("This is a test group for unit tests",)
        member_count = (0,)

        add_group(
            g_name=name,
            m_id=meetup_id,
            photo=photo_url,
            rating=rating,
            descrip=description,
            m_count=member_count,
        )

        # Get group and assert json
        response = session.query(CommunityGroups).filter_by(meetup_id="-4").first()
        self.assertEqual(response.name, "Test Group")
        self.assertEqual(response.meetup_id, -4)
        self.assertEqual(response.photo_url, "No URL provided")
        self.assertEqual(response.rating, 6.00)
        self.assertEqual(response.description, "This is a test group for unit tests")
        self.assertEqual(response.member_count, 0)
        self.assertEqual(response.num_events, -1)

        session.delete(response)
        session.commit()

    def test_add_event_types_fill_2(self):
        name = "Test Type"
        description = ("This is a test type for unit testing",)
        meetup_id = -5
        photo_url = ("No URL provided",)
        add_type(event_name=name, m_id=meetup_id, photo=photo_url, descrip=description)

        # Get e_type and assert json
        response = session.query(EventTypes).filter_by(meetup_id="-5").first()
        self.assertEqual(response.name, "Test Type")
        self.assertEqual(response.description, "This is a test type for unit testing")
        self.assertEqual(response.meetup_id, -5)
        self.assertEqual(response.photo_url, "No URL provided")
        self.assertEqual(response.video_id, "-1")

        session.delete(response)
        session.commit()

    def test_add_event_fill_2(self):
        name = "Test Event"
        e_type = "Test Type"
        cgroup = "Test Group"
        description = "This is a test event for unit testing"
        address = "Test Address"
        city = "Austin"
        state = "TX"
        country = "USA"
        zipcode = "78705"
        time = "Test Date Time"
        photo_url = "No URL provided"
        meetup_id = "-6"

        add_event(
            e_name=name,
            e_type=e_type,
            group=cgroup,
            descrip=description,
            address=address,
            ct=city,
            st=state,
            cy=country,
            zipc=zipcode,
            dt=time,
            photo=photo_url,
            m_id=meetup_id,
        )

        # Get event and assert json
        # Get event and assert json
        response = session.query(AllEvents).filter_by(meetup_id="-6").first()
        self.assertEqual(response.event_name, "Test Event")
        self.assertEqual(response.event_type, "Test Type")
        self.assertEqual(response.com_group, "Test Group")
        self.assertEqual(response.description, "This is a test event for unit testing")
        self.assertEqual(response.addr, "Test Address")
        self.assertEqual(response.city, "Austin")
        self.assertEqual(response.state, "TX")
        self.assertEqual(response.country, "USA")
        self.assertEqual(response.zipcode, "78705")
        self.assertEqual(response.time, "Test Date Time")
        self.assertEqual(response.meetup_id, "-6")
        self.assertEqual(response.photo_url, "No URL provided")

        session.delete(response)
        session.commit()

    # Testing getting informtion from database

    def test_get_event(self):
        response = event_id_handler(4)
        expected_dict = {
            "4": {
                "city": "Phoenix",
                "addr": "455 N 3rd St",
                "event_name": "LOCAL ART CLUB | Wynwood Art Walk",
                "country": "USA",
                "description": "<p>Once a year they have this reception FREE to the public for a new book launching for the kids. Truly, they have great food and beverages! But, the best part is being able to talk with each kid at their photo display - some are painfully shy, some very outgoing and embracing this as a tremendous opportunity. Their photos are FABULOUS! You certainly would never suspect that they were taken by an at-risk child!!!</p> <p>THIS year, it will be held OUTSIDE under the lighted palm trees at the AZ Center, so it should be a fun time!</p> <p>PLEASE OPEN THE PHOTO ATTACHED AS THIS STUPID MEETUP FORMAT USES AN ODD RATIO AND THE PHOTO WITH PARKING INSTRUCTIONS GETS CUT OFF.</p> <p>I cried last year as it was so incredible to see these young children change their lives just by learning how to take photos with a camera! Come celebrate this years' group of kids!</p>",
                "zipcode": "85004",
                "related_events": {
                    "1": {
                        "event_type": "Arts & Culture",
                        "event_name": "What Makes Life Joyful - ABUNDANCE CHAPTER 2",
                        "zipcode": "80123",
                        "time": "Thursday March 28, 2019",
                        "com_group": "Lifelong Learners Group",
                        "id": 2,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/a/5/9/f/global_478662399.jpeg",
                    },
                    "2": {
                        "event_type": "Arts & Culture",
                        "event_name": "Design Museum Mornings: Diversity to Equity, by Design",
                        "zipcode": "97209",
                        "time": "Friday March 29, 2019",
                        "com_group": "Design Museum Portland",
                        "id": 3,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/3/6/1/global_478500865.jpeg",
                    },
                },
                "state": "AZ",
                "event_type_id": 1,
                "time": "Saturday March 30, 2019",
                "com_group_id": 3,
                "com_group": "Art Explorers Meetup",
                "photo_url": "https://secure.meetupstatic.com/photos/event/3/d/1/6/global_479655638.jpeg",
                "event_type": "Arts & Culture",
            }
        }
        resp = response.json
        self.assertEqual(expected_dict["4"]["event_name"], resp["4"]["event_name"])

    def test_get_group(self):
        response = group_id_handler(4)
        expected_dict = {
            "4": {
                "rating": 4.9,
                "description": "<p>Artists, musicians, writers, poets, and art lovers all love to gather at the dA. Our groups purpose is to create, nurture, and expand community by providing a place for cultural exchange through the arts. We host art exhibitions, studio art classes for drawing, painting, printmaking, &amp; clay as well as musical concerts, writing workshops, poetry readings, and theatrical performances. We are a diverse, multi-discipline, non-profit organization who seeks to enhance the quality of life for the greater community by educating and providing opportunities to experience, appreciate and support the arts!</p>",
                "member_count": 3310,
                "community_group_name": "Miami Social - Art, Fashion & House Music",
                "related_events": {
                    "1": {
                        "event_type": "Arts & Culture",
                        "event_name": "Painting Studio Saturdays",
                        "zipcode": "91766",
                        "time": "Saturday March 30, 2019",
                        "com_group": "Miami Social - Art, Fashion & House Music",
                        "id": 5,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/c/8/7/2/global_468711314.jpeg",
                    }
                },
                "photo_url": "https://secure.meetupstatic.com/photos/event/e/1/8/600_437763608.jpeg",
            }
        }
        resp = response.json
        self.assertEqual(
            expected_dict["4"]["community_group_name"],
            resp["4"]["community_group_name"],
        )

    def test_get_type(self):
        response = type_id_handler(4)
        expected_dict = {
            "4": {
                "description": "a road vehicle, typically with four wheels, powered by an internal combustion engine and able to carry a small number of people.<br>a railroad vehicle for passengers or freight.<br>the passenger compartment of an elevator, cableway, airship, or balloon.<br>a chariot.<br>",
                "event_type_name": "Cars & Motorcycles",
                "num_events": 15,
                "id": 4,
                "avg_group_rating": 4.86,
                "related_events": {
                    "1": {
                        "event_type": "Cars & Motorcycles",
                        "event_name": "MYSTERY RIDE",
                        "zipcode": "T2V 3H9",
                        "time": "Friday April 12, 2019",
                        "com_group": "THE CALGARY MOTORCYCLE RIDERS AND EVERYTHING ELSE",
                        "id": 128,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/a/5/a/7/global_471162407.jpeg",
                    },
                    "2": {
                        "event_type": "Cars & Motorcycles",
                        "event_name": "Grassy Waters Preserve: Owahee Trail ",
                        "zipcode": "33411",
                        "time": "Saturday March 30, 2019",
                        "com_group": "Jupiter Bicycling Group",
                        "id": 120,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/9/d/b/9/global_479800377.jpeg",
                    },
                    "3": {
                        "event_type": "Cars & Motorcycles",
                        "event_name": "Pioneer Ghost town Rhyolite and Titus Canyon or Chloride City if Titus is Closed",
                        "zipcode": "89003",
                        "time": "Saturday March 30, 2019",
                        "com_group": '"DESERT WRANGLERS" A Jeep Wrangler Club',
                        "id": 121,
                        "photo_url": "https://secure.meetupstatic.com/photos/event/8/5/3/1/global_479074097.jpeg",
                    },
                },
                "num_groups": 11,
                "photo_url": "https://secure.meetupstatic.com/photos/event/1/3/3/b/highres_431404923.jpeg",
            }
        }
        resp = response.json
        self.assertEqual(
            expected_dict["4"]["event_type_name"], resp["4"]["event_type_name"]
        )

        # Testing main.py methods for error cases

    def test_error_get_event(self):
        response = event_id_handler(-1111)
        expected_dict = {
            "Error": "The given event_id couldn't be found in the database"
        }
        resp = response.json
        self.assertEqual(expected_dict["Error"], resp["Error"])

    def test_error_get_type(self):
        response = type_id_handler(-1111)
        expected_dict = {"Error": "The given type_id couldn't be found in the database"}
        resp = response.json
        self.assertEqual(expected_dict["Error"], resp["Error"])

    def test_error_get_group(self):
        response = group_id_handler(-1111)
        expected_dict = {
            "Error": "The given group_id couldn't be found in the database"
        }
        resp = response.json
        self.assertEqual(expected_dict["Error"], resp["Error"])


if __name__ == "__main__":
    unittest.main()
