import React from 'react';
import { Table } from 'reactstrap';
import Axios from 'axios'
import { privateApiKey, projectID } from "./configuration";

export default class StatsTable extends React.Component {
    constructor() {
        super();
        this.state = {
          front_com: [],
          front_issues: [],
          front_unit_tests: [],
        }
      }

      async componentWillMount() {
          const front_commit_url = "https://gitlab.com/api/v4/projects/" + projectID + "/repository/contributors?sort=desc";
          Axios.get(front_commit_url, {
            params: {
                private_token: privateApiKey
            }})
            .then(response => {
              const data = response.data;
              var commits = [0,0,0,0,0,0];
              
              this.getCommits(data, commits);
              return commits;
            })
            .then(data => {
              this.setState({front_com: data});
            })

        const front_issue_url = "https://gitlab.com/api/v4/projects/" + projectID + "/issues";
        Axios.get(front_issue_url, {
            params: {
                private_token: privateApiKey
            }})
          .then(response => {
            const data = response.data;
            var issues = [0,0,0,0,0,0];
            this.getIssues(data, issues);
            return issues;
          })
          .then(data => {
            this.setState({front_issues: data});
            console.log(this.state.front_issues);
          })
      }

      getCommits(arr, commits) {
          for (var i = 0; i < arr.length; i++) {
              switch(arr[i].name) {
                    case "Nishtha Aggarwal":
                        commits[0] += arr[i].commits;
                        commits[5] += arr[i].commits;
                        break;
                    case "Haarika Somarouthu":
                        commits[1] += arr[i].commits;
                        commits[5] += arr[i].commits;
                        break;
                    case "Raja Venkate Akula Ramesh Kum":
                        commits[2] += arr[i].commits;
                        commits[5] += arr[i].commits;
                        break;
                    case "KaushikKoiralaUT":
                        commits[3] += arr[i].commits;
                        commits[5] += arr[i].commits;
                        break;
                    case "shivamppatel":
                        commits[4] += arr[i].commits;
                        commits[5] += arr[i].commits;
                        break;    
                    default:
                        break;
              }
          }
      }

      getIssues(data, issues) {
        for (var i = 0; i < data.length; i++) {
            if(data[i].assignee != null) {
                if (data[i].assignee.id == 3446892){
                    issues[0] += 1;
                    issues[5] += 1;
                } else if (data[i].assignee.id == 3507608){
                    issues[1] += 1;
                    issues[5] += 1;
                } else if (data[i].assignee.id == 3520045){
                    issues[2] += 1;
                    issues[5] += 1;
                } else if (data[i].assignee.id == 3520696){
                    issues[3] += 1;
                    issues[5] += 1;
                } else if (data[i].assignee.id == 3450835){
                    issues[4] += 1;
                    issues[5] += 1;
                }
            }
        }
    }

    render() {
        return (
        <Table dark striped>
            <thead>
            <tr>
                <th>Member Name</th>
                <th>Commits</th>
                <th>Issues</th>
                <th>Unit Tests</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Nishtha Aggarwal</th>
                <td>{this.state.front_com[0]}</td>
                <td>{this.state.front_issues[0]}</td>
                <td>0</td>
            </tr>
            <tr>
                <th scope="row">Haarika Somarouthu</th>
                <td>{this.state.front_com[1]}</td>
                <td>{this.state.front_issues[1]}</td>
                <td>0</td>
            </tr>
            <tr>
                <th scope="row">Raja Akula</th>
                <td>{this.state.front_com[2]}</td>
                <td>{this.state.front_issues[2]}</td>
                <td>0</td>
            </tr>
            <tr>
                <th scope="row">Kaushik Koirala</th>
                <td>{this.state.front_com[3]}</td>
                <td>{this.state.front_issues[3]}</td>
                <td>0</td>
            </tr>
            <tr>
                <th scope="row">Shivam Patel</th>
                <td>{this.state.front_com[4]}</td>
                <td>{this.state.front_issues[4]}</td>
                <td>0</td>
            </tr>
            <tr>
                <th scope="row">Total</th>
                <td>{this.state.front_com[5]}</td>
                <td>{this.state.front_issues[5]}</td>
                <td>0</td>
            </tr>
        
            </tbody>
        </Table>
        );
  }
}


// WEBPACK FOOTER //
// ./src/components/views/AboutPage/StatsTable.js